package za.co.ideal.ca.adminportal.controllers;

import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.ideal.ca.adminportal.dto.responses.ConfigData;
import za.co.ideal.ca.adminportal.exceptions.GenericException;
import za.co.ideal.ca.adminportal.services.ConfigService;
import za.co.ideal.ca.adminportal.services.SessionService;

import javax.servlet.http.HttpServletRequest;

@CrossOrigin
@RestController
@RequestMapping(value = "ca-portal/api/v1/config")
@Log4j2
public class ConfigController {

    private static final Logger logger = LogManager.getLogger(PlanController.class);
    private final ConfigService configService;
    private final SessionService sessionService;

    public ConfigController(ConfigService configService, SessionService sessionService) {
        this.configService = configService;
        this.sessionService = sessionService;
    }

    @GetMapping("/retrieve")
    private ResponseEntity<ConfigData> retrieveConfigData(HttpServletRequest request) {

        if (!sessionService.isTokenValid(request)) {
            throw new GenericException("Token is invalid", HttpStatus.FORBIDDEN, "Token is invalid");
        }

        ConfigData configData = configService.retrieveConfigData(request);
        return ResponseEntity.ok(configData);
    }

}









