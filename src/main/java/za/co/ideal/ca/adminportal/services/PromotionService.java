package za.co.ideal.ca.adminportal.services;

import org.springframework.data.domain.Page;
import za.co.ideal.ca.adminportal.dto.requests.FilterPlans;
import za.co.ideal.ca.adminportal.dto.requests.PostPromotion;
import za.co.ideal.ca.adminportal.dto.responses.Plan;
import za.co.ideal.ca.adminportal.entities.PromotionsEntity;
import za.co.ideal.ca.adminportal.models.Promotion;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface PromotionService {

    Promotion findById(HttpServletRequest request, long id);

    Page<Promotion> findAll(int page, int limit);

    Promotion savePromotion(HttpServletRequest request, PostPromotion postPromotion);

    List<PromotionsEntity> getPromosForPlan(List<Plan> plans, int page, int limit);

    void deletePromotion(long id);

    void updatePromotion(PostPromotion postPromotion, long id);
}
