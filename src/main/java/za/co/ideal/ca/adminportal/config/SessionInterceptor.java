package za.co.ideal.ca.adminportal.config;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.HandlerInterceptor;
import za.co.ideal.ca.adminportal.config.yaml.SecurityYamlProperties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;

@Service
public class SessionInterceptor implements HandlerInterceptor{

    //unimplemented methods comes here. Define the following method so that it
    //will handle the request before it is passed to the controller.

    private static final Logger logger = LogManager.getLogger(RequestInterceptor.class);
    private final SecurityYamlProperties securityYamlProperties;

    public SessionInterceptor(SecurityYamlProperties securityYamlProperties) {
        this.securityYamlProperties = securityYamlProperties;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        logger.info("Intercepted req!" + request.getRequestURI());
        String accessToken = request.getHeader("Authorization");

        logger.info("Token | " + accessToken);

        if (!request.getRequestURI().contains("getToken")) {
            logger.info("Is not login url, verify!");
            // Verify JWT Token
            Algorithm algorithm = Algorithm.HMAC256(securityYamlProperties.getApiKey().getBytes(StandardCharsets.UTF_8));
            JWTVerifier verifier = JWT.require(algorithm)
                    .build(); //Reusable verifier instance
            try {
                DecodedJWT decodedJWT = verifier.verify(accessToken);
                return true;
            } catch (Exception e) {
                return false;
            }
        }

        logger.info("Is login url, no verify necessary!");
        return true;
    }
}

