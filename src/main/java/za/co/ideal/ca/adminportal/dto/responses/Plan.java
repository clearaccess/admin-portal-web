package za.co.ideal.ca.adminportal.dto.responses;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import za.co.ideal.ca.adminportal.entities.PlansEntity;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Plan {

    private long id;
    private String actions;
    private String connectivityProvider;
    private String installOptions;
    private Boolean onlyPromo;
    private Double price;
    private String serviceDetails;
    private String serviceType;
    private String shortcode;
    private String title;
    private Date dateCreated;
    private String status;

    public Plan(PlansEntity plansEntity) {
        this.id = plansEntity.getId();
        this.actions = plansEntity.getActions();
        this.connectivityProvider = plansEntity.getConnectivityProvider();
        this.installOptions = plansEntity.getInstallOptions();
        this.onlyPromo = plansEntity.getOnlyPromo();
        this.price = plansEntity.getPrice();
        this.serviceDetails = plansEntity.getServiceDetails();
        this.serviceType = plansEntity.getServiceType();
        this.shortcode = plansEntity.getShortcode();
        this.title = plansEntity.getTitle();
    }

    public Plan(SplynxPlanResponse splynxPlanResponse) {
        this.id = splynxPlanResponse.getId();
        this.connectivityProvider = splynxPlanResponse.getAdditionalAttributes().getConnectivityProvider();
        // this.actions = splynxPlanResponse.getAdditionalAttributes();
        this.installOptions = splynxPlanResponse.getAdditionalAttributes().getInstallOptions().toString();
//        this.onlyPromo = splynxPlanResponse.getAdditionalAttributes().getPro;
        this.price = splynxPlanResponse.getPrice();
        this.serviceDetails = splynxPlanResponse.getAdditionalAttributes().getServiceDetails();
        this.serviceType = splynxPlanResponse.getAdditionalAttributes().getServiceType();
        this.shortcode = splynxPlanResponse.getAdditionalAttributes().getTwentyEightEastShortCode();
        this.title = splynxPlanResponse.getTitle();
        this.dateCreated = splynxPlanResponse.getUpdatedAt();
        // TODO: Is available for services correct indicator for if a service is active?
        this.status = splynxPlanResponse.isAvailableForServices() ? "Active" : "Inactive";
    }

//    public Plan(SplynxPlanResponse splynxPlanResponse) {
//        this.id = splynxPlanResponse.getId();
//        this.connectivityProvider = splynxPlanResponse.getServiceName();
//        this.installOptions = splynxPlanResponse.get
//    }

}
