package za.co.ideal.ca.adminportal.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import za.co.ideal.ca.adminportal.entities.UserRoleEntity;
import za.co.ideal.ca.adminportal.entities.UserRolesEntity;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserRole {
    private String role;

    public UserRole (UserRoleEntity userRoleEntity) {
        this.role = userRoleEntity.getUserRolesByUserRolesId().getRole();
    }

    public UserRole(UserRolesEntity userRoles) {
        this.role = userRoles.getRole();
    }
}
