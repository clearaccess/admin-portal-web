package za.co.ideal.ca.adminportal.models.error;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Errors {

    private String errorCode;
    private String message;
    private List<ErrorItem> errorItemList;

}
