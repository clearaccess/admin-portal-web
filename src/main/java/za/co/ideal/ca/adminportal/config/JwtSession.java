package za.co.ideal.ca.adminportal.config;

import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import za.co.ideal.ca.adminportal.models.Token;

@Component
@Scope("session")
@Data
public class JwtSession {

    private Token token;

}
