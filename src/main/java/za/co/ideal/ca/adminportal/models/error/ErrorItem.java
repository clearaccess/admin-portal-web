package za.co.ideal.ca.adminportal.models.error;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ErrorItem {

    private String field;
    private String message;

}
