package za.co.ideal.ca.adminportal.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Login {

    @NotNull
    private String username;
    @NotNull
    private String password;

}
