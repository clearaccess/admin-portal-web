package za.co.ideal.ca.adminportal.dto.responses;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import za.co.ideal.ca.adminportal.dto.UserRole;
import za.co.ideal.ca.adminportal.entities.PortalUserEntity;
import za.co.ideal.ca.adminportal.entities.UserRoleEntity;

import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {

    private String username;
    private List<UserRole> userRoles;

    public User(PortalUserEntity portalUserEntity, List<UserRoleEntity> userRoleEntities) {
        this.username = portalUserEntity.getUsername();
        if (userRoleEntities != null) {
            this.userRoles = userRoleEntities.stream().map(UserRole::new).collect(Collectors.toList());
        }
    }
}
