package za.co.ideal.ca.adminportal.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import za.co.ideal.ca.adminportal.models.error.ErrorItem;
import za.co.ideal.ca.adminportal.models.error.Errors;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@ControllerAdvice
public class GlobalControllerExceptionHandler extends ResponseEntityExceptionHandler {

    public Errors buildErrorMessage(GenericException genericException)  {
        logger.error(genericException.getReason(), genericException);
        List<ErrorItem> listOfErrorItems = new ArrayList<>();
        listOfErrorItems.add(new ErrorItem(genericException.getReason(), genericException.getResponse()));
        return new Errors(genericException.getHttpStatus().toString(), genericException.getReason(), listOfErrorItems);
    }


    @ExceptionHandler(GenericException.class)
    public ResponseEntity<Errors> handleCardAuthorisationException(GenericException genericException) {
        return ResponseEntity.status(genericException.getHttpStatus()).body(buildErrorMessage(genericException));
    }
}
