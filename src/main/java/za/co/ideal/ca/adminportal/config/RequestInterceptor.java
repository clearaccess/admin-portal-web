package za.co.ideal.ca.adminportal.config;

import lombok.extern.log4j.Log4j2;
import org.apache.http.HttpHeaders;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import za.co.ideal.ca.adminportal.dto.responses.AuthResponse;
import za.co.ideal.ca.adminportal.services.AuthServiceImpl;
import za.co.ideal.ca.adminportal.services.PlanServiceImpl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Log4j2
public class RequestInterceptor implements ClientHttpRequestInterceptor {

    private AuthServiceImpl authService;

    private static final Logger logger = LogManager.getLogger(RequestInterceptor.class);

    @Autowired
    public RequestInterceptor(AuthServiceImpl authService) {
        this.authService = authService;
    }

    @Override
    public ClientHttpResponse intercept(
            HttpRequest request,
            byte[] body,
            ClientHttpRequestExecution execution) throws IOException {

        logger.info("INTERCEPT REQUEST");

//        AuthResponse authResponse;
//
//        try {
//            authResponse = this.authService.getAuthToken();
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//            throw new RuntimeException();
//        } catch (InvalidKeyException e) {
//            e.printStackTrace();
//            throw new RuntimeException();
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//            throw new RuntimeException();
//        }
//
//        request.getHeaders().set(
//            HttpHeaders.AUTHORIZATION, "Splynx-EA (access_token=" + authResponse.getAccess_token() + ")"
//        );
//
//        log.info("INTERCEPTED REQUEST...");
//        log.info("getHeaders::: " + request.getHeaders().get("Authorization"));
//        log.info("getHeaders::: " + request.getHeaders().get("AUTHORIZATION"));

        // log.info("" + request.getHeaders().toString());
        return execution.execute(request, body);
    }
}
