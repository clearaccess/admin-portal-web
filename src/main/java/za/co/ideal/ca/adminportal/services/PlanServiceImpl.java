package za.co.ideal.ca.adminportal.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;
import za.co.ideal.ca.adminportal.config.yaml.SplynxYamlProperties;
import za.co.ideal.ca.adminportal.dto.requests.FilterPlans;
import za.co.ideal.ca.adminportal.dto.responses.*;
import za.co.ideal.ca.adminportal.entities.PromotionsEntity;
import za.co.ideal.ca.adminportal.exceptions.GenericException;
import za.co.ideal.ca.adminportal.models.Promotion;
import za.co.ideal.ca.adminportal.repositories.PromotionsRepository;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static za.co.ideal.ca.adminportal.config.ApplicationConstants.GENERIC_FAILURE;

@Service
public class PlanServiceImpl implements PlanService   {

    private static final Logger logger = LogManager.getLogger(PlanServiceImpl.class);

    private final SplynxYamlProperties splynxYamlProperties;
    private final RestTemplate restTemplate;
    private final AuthService authService;
    private final PromotionsRepository promotionsRepository;

    public PlanServiceImpl(SplynxYamlProperties splynxYamlProperties, RestTemplateBuilder restTemplateBuilder, AuthService authService, PromotionsRepository promotionsRepository) {
        this.splynxYamlProperties = splynxYamlProperties;
        this.restTemplate = restTemplateBuilder.build();
        this.authService = authService;
        this.promotionsRepository = promotionsRepository;
    }

    @Override
    public List<SplynxPlanResponse> findAllPlans(HttpServletRequest request) {
        ParameterizedTypeReference<List<SplynxPlanResponse>> responseType = new ParameterizedTypeReference<List<SplynxPlanResponse>>() {};
        ResponseEntity<List<SplynxPlanResponse>> responseEntity;

        AuthResponse authResponse;
        String remoteAddr = "";

        try {
            authResponse = this.authService.getAuthToken();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new RuntimeException();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
            throw new RuntimeException();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            throw new RuntimeException();
        } catch (HttpClientErrorException.Unauthorized e) {
            try {
                authResponse = this.authService.getAuthToken();
            } catch (NoSuchAlgorithmException er) {
                er.printStackTrace();
                throw new RuntimeException();
            } catch (InvalidKeyException er) {
                er.printStackTrace();
                throw new RuntimeException();
            } catch (UnsupportedEncodingException er) {
                er.printStackTrace();
                throw new RuntimeException();
            }
        }

        AuthResponse finalAuthResponse = authResponse;
        restTemplate.getInterceptors().add(
                (outReq, bytes, clientHttpReqExec) -> {
                    outReq.getHeaders().set(
                            org.apache.http.HttpHeaders.AUTHORIZATION, "Splynx-EA (access_token=" + finalAuthResponse.getAccess_token() + ")"
                    );
                    return clientHttpReqExec.execute(outReq, bytes);
                });

        if (request != null) {
            remoteAddr = request.getHeader("X-FORWARDED-FOR");
            if (remoteAddr == null || "".equals(remoteAddr)) {
                remoteAddr = request.getRemoteAddr();
            }
        }

        try {
            logger.info("LIST URL | " + splynxYamlProperties.getTariffs());
            responseEntity = restTemplate.exchange(
                    splynxYamlProperties.getTariffs(),
                    HttpMethod.GET,
                    null,
                    responseType
            );
        } catch (RestClientResponseException e) {
            logger.info(e.getMessage());
            logger.info(e.getResponseBodyAsString());
            logger.info(e.getStatusText());
            throw new GenericException(String.format(GENERIC_FAILURE.value(), "retrieve list of", "promotions"), HttpStatus.valueOf(e.getRawStatusCode()), e.getResponseBodyAsString());
        }

        return responseEntity.getBody();
    }

    @Override
    public Page<PromoPlanPagingResponse> mapPromoPlans(List<Promotion> promotion, int page, int limit, List<Plan> plans1) {
        List<PromoPlanPagingResponse> promoPlanPagingResponse =
                promotion
                        .stream()
                        .map(PromoPlanPagingResponse::new)
                        .collect(Collectors.toList());

        Pageable pageable = PageRequest.of(page, limit);

        logger.info("Page | " + page);
        logger.info("Limit | " + limit);
        logger.info("Count | " + promotionsRepository.count());

        List<PromotionsEntity> promotionsEntities = promotionsRepository.findAllByPlanIdInCustom(
                        plans1.
                                stream()
                                .map(Plan::getId)
                                .collect(Collectors.toList())
                )
                .stream()
                .sorted(Comparator.comparingLong(PromotionsEntity::getId))
                .collect(Collectors.toList());

        logger.info("ERROR BEFORE HERE?");

        final Page<PromoPlanPagingResponse> pageableObj = new PageImpl<>(promoPlanPagingResponse, pageable, promotionsEntities.size());
        return pageableObj;
                // can't page cuz not jpa D:
    }

//    @Override
//    public Plan findPlanById(long id) {
//
//
//    }
//
//    @Override
//    public List<Plan> filterPlans(FilterPlans filterPlans) {
//
//
//
//    }

    @Override
    public List<Plan> getPlansForPromo(HttpServletRequest request, List<PromotionsEntity> promotionsEntity) {

        List<Long> promoIds = promotionsEntity
                .stream()
                .map(PromotionsEntity::getPlanId)
                .collect(Collectors.toList());

        List<SplynxPlanResponse> splynxPlanResponses = this.findAllPlans(request);

        List<Plan> filteredPlans = splynxPlanResponses
                .stream()
                .filter(splynxPlanResponse -> {
                    logger.info("PLAN ID | " + splynxPlanResponse.getId());
//                    logger.info("PROMO ID | " + promotionsEntity.getPlanId());

                    if (promoIds.contains(splynxPlanResponse.getId())) {
                        return true;
                    }

                    return false;

//                    return splynxPlanResponse.getId() == promotionsEntity.getPlanId();
                })
                .collect(Collectors.toList())
                .stream()
                .map(Plan::new)
                .collect(Collectors.toList());

        return filteredPlans;

    }

    @Override
    public List<Plan> filterPlans(FilterPlans filterPlans, HttpServletRequest request) {

        List<SplynxPlanResponse> splynxPlanResponses = this.findAllPlans(request);

        List<Plan> plans = new ArrayList<>();


//        List<Person> beerDrinkers = persons.stream()
//                .filter(p -> p.getAge() > 16).collect(Collectors.toList());

        splynxPlanResponses
                .stream()
                .map(Plan::new)
                .forEach(plan -> {
                    if (filterPlans.getServiceProviders() != null && filterPlans.getServiceProviders().size() > 0 && filterPlans.getServiceTypes() != null && filterPlans.getServiceTypes().size() > 0) {
                        // Both filter types
                        logger.info("Both Filters null NOT | 0");
                        if (filterPlans.getServiceTypes().contains(plan.getServiceType()) && filterPlans.getServiceProviders().contains(plan.getConnectivityProvider())) {
                            logger.info("I AM BEING RUN | 1");
                            logger.info("ADDING Both filter types Plan | " + plan.toString());
                            plans.add(plan);
                        }
                    }

                 else if (filterPlans.getServiceProviders() != null && filterPlans.getServiceProviders().size() > 0) {
                      // Only service providers filter
                      logger.info("Get Providers null | 2");
                      if (filterPlans.getServiceProviders().contains(plan.getConnectivityProvider())) {
                          logger.info("ADDING Only service providers filter Plan | " + plan.toString());
                          plans.add(plan);
                      }
                  }
                  else if (filterPlans.getServiceTypes() != null && filterPlans.getServiceTypes().size() > 0) {
                        // Only service types filter
                        logger.info("Get Providers null |  3");
                        if (filterPlans.getServiceTypes().contains(plan.getServiceType())) {
                            logger.info("ADDING Only service types filter Plan | " + plan.toString());
                            plans.add(plan);
                        }
                    }
                    if ( (filterPlans.getServiceProviders() == null || filterPlans.getServiceProviders().size() == 0) && (filterPlans.getServiceTypes() == null || filterPlans.getServiceTypes().size() == 0)) {
                        // Both service providers and service type are null
                        logger.info("Get Providers & Service null | 4");
                        logger.info("ADDING Both service providers and service type are null Plan | " + plan.toString());
                        plans.add(plan);
                    }

                });

        return plans;

    }




        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////




//        splynxPlanResponses
//                .stream()
//                .map(Plan::new)
//                .forEach(plan -> {
//                    if (filterPlans.getServiceProviders() != null && filterPlans.getServiceProviders().size() > 0 && filterPlans.getServiceTypes() != null && filterPlans.getServiceTypes().size() > 0) {
//                        // Both filter types
//                        logger.info("I AM BEING RUN | 0");
//                        if (filterPlans.getServiceTypes().contains(plan.getServiceType()) && filterPlans.getServiceProviders().contains(plan.getConnectivityProvider())) {
//                            logger.info("I AM BEING RUN | 1");
//                            logger.info("ADDING Both filter types Plan | " + plan.toString());
//                            plans.add(plan);
//                        }
//                    }
//                    if (filterPlans.getServiceProviders() != null && filterPlans.getServiceProviders().size() > 0) {
//                        // Only service providers filter
//                        if (filterPlans.getServiceProviders().contains(plan.getConnectivityProvider())) {
//                            logger.info("ADDING Only service providers filter Plan | " + plan.toString());
//                            plans.add(plan);
//                        }
//                    }
//                    if (filterPlans.getServiceTypes() != null && filterPlans.getServiceTypes().size() > 0) {
//                        // Only service types filter
//                        if (filterPlans.getServiceTypes().contains(plan.getServiceType())) {
//                            logger.info("ADDING Only service types filter Plan | " + plan.toString());
//                            plans.add(plan);
//                        }
//                    }
//                    if ( (filterPlans.getServiceProviders() == null || filterPlans.getServiceProviders().size() == 0) && (filterPlans.getServiceTypes() == null || filterPlans.getServiceTypes().size() == 0)) {
//                        // Both service providers and service type are null
//                        logger.info("ADDING Both service providers and service type are null Plan | " + plan.toString());
//                        plans.add(plan);
//                    }
//
//                    });
//
//        return plans;
//
//    }


}










