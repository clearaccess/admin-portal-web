package za.co.ideal.ca.adminportal.entities;

import lombok.*;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Data
@Entity
@NoArgsConstructor
@Table(name = "plans", schema = "clearaccess_staging", catalog = "clearaccess")
public class PlansEntity {
    @Id@Column(name = "id")
    private int id;
    @Basic@Column(name = "actions")
    private String actions;
    @Basic@Column(name = "connectivity_provider")
    private String connectivityProvider;
    @Basic@Column(name = "install_options")
    private String installOptions;
    @Basic@Column(name = "only_promo")
    private Boolean onlyPromo;
    @Basic@Column(name = "price")
    private Double price;
    @Basic@Column(name = "service_details")
    private String serviceDetails;
    @Basic@Column(name = "service_type")
    private String serviceType;
    @Basic@Column(name = "shortcode")
    private String shortcode;
    @Basic@Column(name = "title")
    private String title;
//    @OneToMany(mappedBy = "plansByPlanId")
//    private Collection<PromotionsEntity> promotionsById;

}
