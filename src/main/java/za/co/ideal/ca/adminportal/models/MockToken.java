package za.co.ideal.ca.adminportal.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import za.co.ideal.ca.adminportal.entities.PortalUserEntity;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MockToken {

    private String access_token;
    private String token_type;
    private int userId;

    public MockToken(Token token, PortalUserEntity portalUserEntity) {
        this.access_token = token.getAccess_token();
        if (token.getUserId() != null) {
            this.userId = Integer.parseInt(token.getUserId());
        }
    }

}
