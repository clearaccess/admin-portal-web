package za.co.ideal.ca.adminportal.controllers;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import za.co.ideal.ca.adminportal.config.JwtSession;
import za.co.ideal.ca.adminportal.config.yaml.SecurityYamlProperties;
import za.co.ideal.ca.adminportal.dto.Login;
import za.co.ideal.ca.adminportal.dto.UserRole;
import za.co.ideal.ca.adminportal.entities.PortalUserEntity;
import za.co.ideal.ca.adminportal.entities.UserRoleEntity;
import za.co.ideal.ca.adminportal.exceptions.GenericException;
import za.co.ideal.ca.adminportal.models.MockToken;
import za.co.ideal.ca.adminportal.models.Token;
import za.co.ideal.ca.adminportal.repositories.PortalUserRepository;
import za.co.ideal.ca.adminportal.repositories.UserRoleRepository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping(value = "ca-portal/api/v1/login-uri")
@Log4j2
public class LoginController {

    private static final Logger logger = LogManager.getLogger(LoginController.class);

    private final SecurityYamlProperties securityYamlProperties;
    private final PortalUserRepository portalUserRepository;
    private final UserRoleRepository userRoleRepository;

    public LoginController(SecurityYamlProperties securityYamlProperties, PortalUserRepository portalUserRepository, UserRoleRepository userRoleRepository) {
        this.securityYamlProperties = securityYamlProperties;
        this.portalUserRepository = portalUserRepository;
        this.userRoleRepository = userRoleRepository;
    }

    @PostMapping("/getToken")
    ResponseEntity<MockToken> getToken(@Valid @RequestBody Login login, HttpServletRequest servletRequest) throws UnsupportedEncodingException {
        logger.info("In Controller!");
        // Replace below with new radius logic.
        if (login.getUsername().equalsIgnoreCase("admin@email.com") && login.getPassword().equals("mFaeh66RWD8Ri2")) {
            JwtSession jwtSession = new JwtSession();
            PortalUserEntity portalUserEntity = portalUserRepository.findByUsername("admin@email.com")
                .orElseThrow(() -> new GenericException(String.format("no user found with username %s"), HttpStatus.NOT_FOUND, String.format("no user found with username %s")));
            // Generate jwt token based on user, roles]
            List<UserRoleEntity> userRoleEntities = userRoleRepository.findAllByPortalUserByUserId(portalUserEntity)
                    .orElseThrow(() -> new GenericException(String.format("no roles found for user with username %s"), HttpStatus.NOT_FOUND, String.format("no roles found for user with username %s")));

            List<UserRole> userRoles = userRoleEntities.stream().map(UserRole::new).collect(Collectors.toList());
            List<String> userRoleStringList = userRoles.stream().map(UserRole::getRole).collect(Collectors.toList());
            String[] userRoleListString = userRoleStringList.toArray(new String[0]);

            String jwtToken = createJWT(getHttpSession().getId(), securityYamlProperties.getApiIdentifier(), (60*60*1000), userRoleListString);
            // Generate session
            // Verify session is valid on each call
            // Decrypt token to check for roles
            jwtSession.setToken(new Token(jwtToken, String.valueOf(portalUserEntity.getId()), userRoles));
            HttpSession httpSession = getHttpSession();
            httpSession.setAttribute("token", jwtSession);

            MockToken mockToken = new MockToken();
            mockToken.setAccess_token(jwtToken);

            logger.info("Token | UNIQUE " + jwtToken);
            JwtSession jwtSessionMock = (JwtSession) httpSession.getAttribute("token");
            logger.info("Jwt session | " + jwtSessionMock.getToken().getAccess_token());


            return ResponseEntity.ok(mockToken);
        } else {
            throw new GenericException("Invalid username/password combination", HttpStatus.UNAUTHORIZED, "Invalid username/password combination");
        }
    }

    public static HttpSession getHttpSession() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        return attr.getRequest().getSession(true);
    }

    public String createJWT(String id, String issuer, long ttlMillis, String[] userRoleList) {
        Algorithm algorithm = Algorithm.HMAC256(securityYamlProperties.getApiKey().getBytes(StandardCharsets.UTF_8));
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        String token = JWT.create()
                .withIssuer(issuer)
                .withJWTId(id)
                .withIssuedAt(now)
                .withArrayClaim("roles", userRoleList)
                .withClaim("exp", now.toInstant().getEpochSecond() + ttlMillis)
                .withExpiresAt(new Date(nowMillis + ttlMillis))
                .sign(algorithm);

        return token;
    }

}
