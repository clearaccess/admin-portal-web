//package za.co.ideal.ca.adminportal.config;
//
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.boot.jdbc.DataSourceBuilder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.HttpMethod;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
//import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfiguration;
//import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//import za.co.ideal.ca.adminportal.entities.User;
//import za.co.ideal.ca.adminportal.repositories.UserRepository;
//
//import javax.sql.DataSource;
//
//@Configuration
//@EnableResourceServer
//@EnableGlobalMethodSecurity(prePostEnabled = true)
//public class ResourceConfig extends ResourceServerConfigurerAdapter implements WebMvcConfigurer {
//
////    @Bean
////    public DataSource getDataSource() {
////        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
////        dataSourceBuilder.username("SA");
////        dataSourceBuilder.password("");
////        return dataSourceBuilder.build();
////    }
//
//    @Override
//    public void configure(HttpSecurity http) throws Exception {
//
////        ArrayList<String> allowedOrigins = new ArrayList<>();
////        allowedOrigins.add("http://localhost:4200");//Adding object in arraylist
//
//        http.cors()
//                .and()
//                .authorizeRequests()
//                .antMatchers(HttpMethod.GET, "/user/info", "/api/foos/**")
//                .hasAuthority("SCOPE_read")
//                .antMatchers(HttpMethod.POST, "/api/foos")
//                .hasAuthority("SCOPE_write")
//                .anyRequest()
//                .authenticated()
//                .and()
//                .oauth2ResourceServer()
//                .jwt();
////        http
////                .cors().configurationSource(corsConfigurationSource());/*.applyPermitDefaultValues()*///);
////        http
////                .sessionManagement()
////                //.invalidSessionUrl("http://localhost:4200/login")
////                .sessionFixation().migrateSession()
////                .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
////                .maximumSessions(1);
//        // .expiredUrl("http://localhost:4200/login");
//    }
//
////    @Bean
////    public CommandLineRunner run(UserRepository userRepository) throws Exception {
////        return (String[] args) -> {
////            User user1 = new User("John", "john@domain.com");
////            User user2 = new User("Julie", "julie@domain.com");
////            userRepository.save(user1);
////            userRepository.save(user2);
////            userRepository.findAll().forEach(user -> System.out.println(user));
////        };
////    }
//
//}
