package za.co.ideal.ca.adminportal.dto.requests;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AuthRequest {
    private String auth_type;
    private String key;
    private String secret;
    private String signature;
    private long nonce;
}
