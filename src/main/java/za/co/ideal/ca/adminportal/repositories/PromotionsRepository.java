package za.co.ideal.ca.adminportal.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import za.co.ideal.ca.adminportal.entities.PromotionsEntity;

import java.util.List;
import java.util.Optional;

public interface PromotionsRepository extends JpaRepository<PromotionsEntity, Long>, PagingAndSortingRepository<PromotionsEntity, Long> {

    List<PromotionsEntity> findAllByPlanIdIn(List<Long> planIds, Pageable pageable);

    @Query("SELECT COUNT(u) FROM PromotionsEntity u WHERE u.planId in (:planIds)")
    long countPromotionsForPlanIds(@Param("planIds") List<Long> planIds);

    Optional<List<PromotionsEntity>> findAllByPlanIdIn(List<Long> planIds);

    @Query("SELECT u FROM PromotionsEntity u WHERE u.planId in (:planIds)")
    List<PromotionsEntity> findAllByPlanIdInCustom(List<Long> planIds);

    PromotionsEntity findByIdAndPlanId(long id, long planId);

    List<PromotionsEntity> findAllByPromoCodeAndPromoOfferAndPromoType(String promoCode, String promoOffer, String promoType);

}
