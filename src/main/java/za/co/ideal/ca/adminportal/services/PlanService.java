package za.co.ideal.ca.adminportal.services;

import org.springframework.data.domain.Page;
import za.co.ideal.ca.adminportal.dto.requests.FilterPlans;
import za.co.ideal.ca.adminportal.dto.responses.Plan;
import za.co.ideal.ca.adminportal.dto.responses.PromoPlanPagingResponse;
import za.co.ideal.ca.adminportal.dto.responses.SplynxPlanResponse;
import za.co.ideal.ca.adminportal.entities.PromotionsEntity;
import za.co.ideal.ca.adminportal.models.Promotion;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface PlanService {

    List<SplynxPlanResponse> findAllPlans(HttpServletRequest request);

    Page<PromoPlanPagingResponse> mapPromoPlans(List<Promotion> promotion, int page, int limit, List<Plan> plans1);

    List<Plan> getPlansForPromo(HttpServletRequest request, List<PromotionsEntity> promotionsEntity);

    List<Plan> filterPlans(FilterPlans filterPlans, HttpServletRequest request);

//    Plan findPlanById(long id);
//
//    List<Plan> filterPlans(FilterPlans filterPlans);

}
