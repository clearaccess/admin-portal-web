package za.co.ideal.ca.adminportal.controllers;

import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import za.co.ideal.ca.adminportal.dto.requests.FilterPlans;
import za.co.ideal.ca.adminportal.dto.responses.Plan;
import za.co.ideal.ca.adminportal.exceptions.GenericException;
import za.co.ideal.ca.adminportal.services.PlanService;
import za.co.ideal.ca.adminportal.services.PromotionService;
import za.co.ideal.ca.adminportal.services.SessionService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "ca-portal/api/v1/plan")
@Log4j2
public class PlanController {

    private final PlanService planService;
    private final PromotionService promotionService;
    private final SessionService sessionService;
    private static final Logger logger = LogManager.getLogger(PlanController.class);

    public PlanController(PlanService planService, PromotionService promotionService, SessionService sessionService) {
        this.planService = planService;
        this.promotionService = promotionService;
        this.sessionService = sessionService;
    }

//    @GetMapping("/{page}/{limit}")
//    public ResponseEntity<Page<PromoPlanPagingResponse>> findAllPlans(HttpServletRequest request, @NonNull @PathVariable int page,  @NonNull @PathVariable int limit) {
//        try {
//            List<SplynxPlanResponse> plans = planService.findAllPlans(request);
//            List<Plan> plans1 = plans
//                    .stream()
//                    .map(Plan::new)
//                    .collect(Collectors.toList());
//            logger.info("Mapped plans from SplynxPlanResponse: " + plans1.toString());
//
//            List<PromotionsEntity> promotionsEntities = promotionService.getPromosForPlan(plans1, page, limit);
//            List<Promotion> promotions = promotionsEntities
//                    .stream()
//                    .map(promotionsEntity -> new Promotion(promotionsEntity, plans1.get((int) promotionsEntity.getPlanId())))
//                    .collect(Collectors.toList());
//
//            return ResponseEntity.ok(planService.mapPromoPlans(promotions));
//        } catch (Exception e) {
//            throw new GenericException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
//        }
//
////        List<SplynxPlanResponse> = new
//    }

    @PostMapping("/filter")
    public ResponseEntity<List<Plan>> filterPlans(@RequestBody @Validated FilterPlans filterPlans, HttpServletRequest request) {

        if (!sessionService.isTokenValid(request)) {
            throw new GenericException("Token is invalid", HttpStatus.FORBIDDEN, "Token is invalid");
        }

        return ResponseEntity.ok(planService.filterPlans(filterPlans, request));
    }

}
