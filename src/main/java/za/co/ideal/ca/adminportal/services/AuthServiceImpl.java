package za.co.ideal.ca.adminportal.services;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import za.co.ideal.ca.adminportal.config.yaml.SplynxAuthYamlProperties;
import za.co.ideal.ca.adminportal.config.yaml.SplynxYamlProperties;
import za.co.ideal.ca.adminportal.dto.requests.AuthRequest;
import za.co.ideal.ca.adminportal.dto.responses.AuthResponse;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

@Service
@Log4j2
public class AuthServiceImpl implements AuthService {

    String access_token;
    long access_token_expiration;
    String refresh_token;
    long refresh_token_expiration;

    long nonce = new Date().getTime()/1000;

    private final SplynxYamlProperties splynxYamlProperties;
    private final SplynxAuthYamlProperties splynxAuthYamlProperties;
    private final RestTemplate restTemplate;

    @Autowired
    public AuthServiceImpl(SplynxYamlProperties splynxYamlProperties, SplynxAuthYamlProperties splynxAuthYamlProperties, RestTemplateBuilder restTemplate) {
        this.splynxYamlProperties = splynxYamlProperties;
        this.splynxAuthYamlProperties = splynxAuthYamlProperties;
        this.restTemplate = restTemplate.build();
    }

    public AuthResponse getAuthToken() throws NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {
        AuthRequest authRequest = new AuthRequest();
        authRequest.setAuth_type("api_key");
        String signature = this.nonce + splynxAuthYamlProperties.getApiKey();
        byte[] hash = toHmacSHA256(signature, splynxAuthYamlProperties.getSecret());
        String hashHexed = toHex(hash);
        authRequest.setNonce(this.nonce++);
        authRequest.setSignature(hashHexed.toUpperCase());
        authRequest.setKey(splynxAuthYamlProperties.getApiKey());
        // Secret is unset before request in reference PHP implementation..
        // Looks like its not explicitly needed in the request body, but is needed for the signature generation.
        // authRequest.setSecret("7d0e13c4234920691e83a5f13b774a6f");
        // log.info("" + authRequest);

        log.info("PROD DEBUG authUrl: " + splynxYamlProperties.getAuth());

        if (this.access_token != null && this.refresh_token != null) {
            if ((new Date().getTime()/1000) + 5 < this.refresh_token_expiration) {
                if (new Date().getTime()/1000 + 5 > this.access_token_expiration) {
                    log.info("Using refresh token...");
                    ResponseEntity<AuthResponse> response = restTemplate.getForEntity(splynxYamlProperties.getAuth() + '/' + this.refresh_token, AuthResponse.class);
                    if (response.getStatusCode().is2xxSuccessful()) {
                        AuthResponse authResponse = response.getBody();
                        this.access_token = authResponse.getAccess_token();
                        this.access_token_expiration = authResponse.getAccess_token_expiration();
                        this.refresh_token = authResponse.getRefresh_token();
                        this.refresh_token_expiration = authResponse.getRefresh_token_expiration();
                        return authResponse;
                    } else {
                        throw new RuntimeException("unable to login");
                    }
                } else {
                    log.info("Using existing token");
                    AuthResponse authResponse = new AuthResponse();
                    authResponse.setAccess_token(this.access_token);
                    authResponse.setAccess_token_expiration(this.access_token_expiration);
                    authResponse.setRefresh_token(this.refresh_token);
                    authResponse.setRefresh_token_expiration(this.refresh_token_expiration);
                    return authResponse;
                }
            } else {
                ResponseEntity<AuthResponse> response = restTemplate.postForEntity(splynxYamlProperties.getAuth(), authRequest, AuthResponse.class);
                if (response.getStatusCode().is2xxSuccessful()) {
                    log.info("Using new token...");
                    AuthResponse authResponse = response.getBody();
                    this.access_token = authResponse.getAccess_token();
                    this.access_token_expiration = authResponse.getAccess_token_expiration();
                    this.refresh_token = authResponse.getRefresh_token();
                    this.refresh_token_expiration = authResponse.getRefresh_token_expiration();
                    return authResponse;
                } else {
                    throw new RuntimeException("unable to login");
                }
            }
        } else {
            log.info("Using new token...");
            ResponseEntity<AuthResponse> response = restTemplate.postForEntity(splynxYamlProperties.getAuth(), authRequest, AuthResponse.class);
            if (response.getStatusCode().is2xxSuccessful()) {
                AuthResponse authResponse = response.getBody();
                this.access_token = authResponse.getAccess_token();
                this.access_token_expiration = authResponse.getAccess_token_expiration();
                this.refresh_token = authResponse.getRefresh_token();
                this.refresh_token_expiration = authResponse.getRefresh_token_expiration();
                return authResponse;
            } else {
                throw new RuntimeException("unable to login");
            }
        }
    }

    private String toHex(byte[] value) {
        String hexed = String.format("%040x", new BigInteger(1, value));
        return hexed;
    }

    private byte[] toHmacSHA256(String value, String sk) {
        byte[] hash = null;
        try {
            SecretKey secretKey = new SecretKeySpec(sk.getBytes("UTF-8"), "HmacSHA256");
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(secretKey);
            hash = mac.doFinal(value.getBytes("UTF-8"));

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return hash;
    }
}
