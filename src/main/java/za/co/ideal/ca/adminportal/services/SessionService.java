package za.co.ideal.ca.adminportal.services;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import za.co.ideal.ca.adminportal.config.RequestInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class SessionService {

    private static final Logger logger = LogManager.getLogger(SessionService.class);

    public boolean isTokenValid(HttpServletRequest request) {
        logger.info("Intercepted req!" + request.getRequestURI());
        String accessToken = request.getHeader("Authorization");

        logger.info("Token | " + accessToken);

        if (!request.getRequestURI().contains("getToken")) {
            logger.info("Is not login url, verify!");
            // Verify JWT Token
            String apiKey = "u9G496Ep4EXaAk";
            String apiIdentifier = "emB5652JGkXoEf";
            Algorithm algorithm = Algorithm.HMAC256(apiKey.getBytes(StandardCharsets.UTF_8));
            logger.info("Algorithm");
            JWTVerifier verifier = JWT.require(algorithm)
                    .build(); //Reusable verifier instance
            logger.info("Verifier");
            try {
                DecodedJWT decodedJWT = verifier.verify(accessToken.replace("Bearer ", ""));
                logger.info("Decoded jwt");
                return true;
            } catch (Exception e) {
                return false;
            }
        }

        logger.info("Is login url, no verify necessary!");
        return true;
    }

    public List<String> getRouteConfig(HttpServletRequest request, HttpSession session) {
        String jwt = "";
        List<String> staticRouteList = new ArrayList<>(Arrays.asList("PROMOTION_MANAGER"));

        // filter staticRouteList with jwt permission claim asList
        return staticRouteList;
    }

}
