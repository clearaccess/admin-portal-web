package za.co.ideal.ca.adminportal.dto.responses;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import za.co.ideal.ca.adminportal.dto.UserRole;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ConfigData {

    private List<String> promoTypes;
    private List<String> serviceTypes;
    private List<String> serviceProviders;
    private List<UserRole> userRoles;

}
