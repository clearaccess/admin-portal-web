package za.co.ideal.ca.adminportal.entities;

import lombok.*;
import za.co.ideal.ca.adminportal.dto.UserRole;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@Table(name = "user_role", schema = "clearaccess_staging", catalog = "clearaccess")
public class UserRoleEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    @ManyToOne@JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    private PortalUserEntity portalUserByUserId;
    @ManyToOne@JoinColumn(name = "user_roles_id", referencedColumnName = "id", nullable = false)
    private UserRolesEntity userRolesByUserRolesId;

    public UserRoleEntity(UserRolesEntity userRoles, PortalUserEntity portalUserEntity) {
        this.portalUserByUserId = portalUserEntity;
        this.userRolesByUserRolesId = userRoles;
    }
}
