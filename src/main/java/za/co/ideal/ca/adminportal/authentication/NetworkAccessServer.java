//package za.co.ideal.ca.adminportal.authentication;
//
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.springframework.beans.factory.annotation.Value;
//import org.tinyradius.packet.AccessRequest;
//import org.tinyradius.packet.RadiusPacket;
//import org.tinyradius.util.RadiusClient;
//import org.tinyradius.util.RadiusException;
//import za.co.ideal.ca.adminportal.config.yaml.RadiusYamlProperties;
//
//import java.io.IOException;
//import java.net.InetAddress;
//import java.net.SocketException;
//
//public class NetworkAccessServer {
//
//    private static final String NAS_IP_ADDRESS = "NAS-IP-Address";
//    private static final String NAS_PORT_ID = "NAS-Port-Id";
//    private RadiusClient radiusClient;
//
//    private static final Logger logger = LogManager.getLogger(NetworkAccessServer.class);
//
////    @Value("${application.properties.radius.serverIp}")
////    private String serverIp;
//
//    public NetworkAccessServer(RadiusServer radiusServer) {
//        this.radiusClient = initRadiusClient(radiusServer);
//    }
//
//    private RadiusClient initRadiusClient(RadiusServer radiusServer)  {
//        try {
//            logger.info(String.format("Attempting to connect to radius server with ip: %s", radiusServer.getIp()));
//            RadiusClient radiusClient = new RadiusClient(radiusServer.getIp(), radiusServer.getSecret());
//            // Set SO Timeout in milliseconds
//            radiusClient.setSocketTimeout(radiusServer.getTimeout());
//            return radiusClient;
//        } catch (SocketException e) {
//            throw new IllegalStateException(e);
//        }
//    }
//
//    public RadiusPacket authenticate(String login, String password) throws IOException, RadiusException {
//
//        System.out.println("Authenticate!");
//        logger.info(String.format("Attempting to login with username %s and password %s", login, password));
//
//        AccessRequest ar = new AccessRequest(login, password);
//
//        ar.setAuthProtocol(AccessRequest.AUTH_PAP);
//
//        ar.addAttribute(NAS_PORT_ID, InetAddress.getLocalHost().getHostAddress());
//
//        ar.addAttribute(NAS_IP_ADDRESS, "127.0.0.1");
//
//        RadiusPacket response = radiusClient.authenticate(ar);
//        return response;
//    }
//
//}
