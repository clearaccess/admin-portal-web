package za.co.ideal.ca.adminportal.controllers;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientResponseException;
import za.co.ideal.ca.adminportal.config.JwtSession;
import za.co.ideal.ca.adminportal.dto.requests.CheckAuthorities;
import za.co.ideal.ca.adminportal.exceptions.GenericException;
import za.co.ideal.ca.adminportal.models.Token;
import za.co.ideal.ca.adminportal.services.SessionService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "/ca-portal/api/v1/session")
public class SessionController {

    private static final Logger logger = LogManager.getLogger(SessionController.class);

    private final SessionService sessionService;

    public SessionController(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    @GetMapping("/retrieve")
    ResponseEntity<Token> retrieveSession(HttpSession session, HttpServletRequest servletRequest) {
        logger.info("Attempting to return session for session id | " + session.getId());
        try {
            JwtSession jwtSession = (JwtSession) session.getAttribute("token");
            logger.info("Jwt session retrieval SERVLET | " + servletRequest.getSession().getId());
            if (jwtSession == null) {
                throw new GenericException("Could not find session, please log in", HttpStatus.FORBIDDEN, "No session exists with id | " + session.getId());
            }
            logger.info("Session found for id | " + session.getId());
            Token token = jwtSession.getToken();
            return ResponseEntity.ok(token);
        } catch (RestClientResponseException e) {
            throw new GenericException("Could not find session, please log in", HttpStatus.FORBIDDEN, "No session exists with id | " + session.getId());
        }
    }

    @PostMapping("/check-authorities")
    ResponseEntity<?> checkSessionAuthorities(@RequestBody @Validated CheckAuthorities checkAuthorities, HttpSession session) {
        try {
            JwtSession jwtSession = (JwtSession) session.getAttribute("token");
            if (jwtSession == null) {
                throw new GenericException("Could not find session, please log in", HttpStatus.FORBIDDEN, "No session exists with id | " + session.getId());
            }
            Token token = jwtSession.getToken();
            if (!isRoutePermitted(checkAuthorities.getRoute(), token)) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (RestClientResponseException e) {
            throw new GenericException("Could not find session, please log in", HttpStatus.FORBIDDEN, "No session exists with id | " + session.getId());
        }
    }

    @GetMapping("/routes")
    ResponseEntity<?> getRouteConfig(HttpServletRequest httpServletRequest, HttpSession session) {
        try {
            return ResponseEntity.ok(this.sessionService.getRouteConfig(httpServletRequest, session));
        } catch (Exception e) {
            throw new GenericException("Could not find session, please log in", HttpStatus.FORBIDDEN, "No session exists with id | " + session.getId());
        }
    }

    @GetMapping("/logout")
    ResponseEntity<?> logout(HttpSession session) {
        try {
            session.removeAttribute("token");
            session.invalidate();
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (RestClientResponseException e) {
            throw new GenericException("Could not find session, please log in", HttpStatus.FORBIDDEN, "No session exists with id | " + session.getId());
        }
    }

    public boolean isRoutePermitted(String route, Token token) {
        JWT jwt = new JWT();
        DecodedJWT decodedJWT = jwt.decodeJwt(token.getAccess_token());
        List<String> roles = decodedJWT.getClaim("roles")
                .asList(String.class);
        switch (route) {
            case "home": {
                return roles.size() > 0;
            }
            case "promotion-manager": {
                return roles.stream().anyMatch(s -> s.equalsIgnoreCase("PROMOTION_MANAGER"));
            }
            default: return false;
        }
    }

}
