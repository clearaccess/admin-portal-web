package za.co.ideal.ca.adminportal.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.ideal.ca.adminportal.config.JwtSession;
import za.co.ideal.ca.adminportal.dto.RouteConfig;
import za.co.ideal.ca.adminportal.exceptions.GenericException;
import za.co.ideal.ca.adminportal.services.SessionService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
@CrossOrigin
@RequestMapping(value = "auth")
public class AuthenticationController {

    private final Logger logger = LogManager.getLogger(AuthenticationController.class);
    private final SessionService sessionService;

    public AuthenticationController(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    @GetMapping
    public ResponseEntity<RouteConfig> getSession(HttpServletRequest httpServletRequest, HttpSession httpSession) {

        if (!sessionService.isTokenValid(httpServletRequest)) {
            throw new GenericException("Token is invalid", HttpStatus.FORBIDDEN, "Token is invalid");
        }

        logger.warn("Http Servlet request details: " + httpServletRequest.toString());
        JwtSession jwtSession = (JwtSession) httpSession.getAttribute("token");
        if (jwtSession == null) {
            throw new GenericException("Could not identify session", HttpStatus.NOT_FOUND, "Could not find any session for this user | ");
        }
        logger.warn("Session cookie details!!! | " + jwtSession.getToken().getAccess_token());
        logger.warn("Session cookie details!!! | " + jwtSession.getToken().getUserId());
        logger.warn("Session cookie details!!! | " + jwtSession.getToken().getUserRoles());

        return ResponseEntity.ok(new RouteConfig(jwtSession.getToken().getUserRoles()));

    }

}
