package za.co.ideal.ca.adminportal.config;

public enum ApplicationConstants {

    GENERIC_FAILURE("Failed to %s %s");

    private final String value;

    ApplicationConstants(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }

    public static ApplicationConstants fromValue(String v) {
        for (ApplicationConstants b : ApplicationConstants.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }

}
