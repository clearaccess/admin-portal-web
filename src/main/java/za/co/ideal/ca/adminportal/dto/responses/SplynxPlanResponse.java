package za.co.ideal.ca.adminportal.dto.responses;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import za.co.ideal.ca.adminportal.entities.PlansEntity;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SplynxPlanResponse {

    // TODO:  Map all responses, then map SplynxResponse to Plans class, then link plans to promo programmatically.

    private long id;
    private String title;
    @JsonProperty("service_name")
    private String serviceName;
    private List<Integer> partnersIds;
    private Double price;
    @JsonProperty("vat_percent")
    private Double vatPercent;
    @JsonProperty("speed_download")
    private int speedDownload;
    @JsonProperty("speed_upload")
    private int speedUpload;
    @JsonProperty("speed_limit_at")
    private int speedLimitAt;
    @JsonProperty("speed_limit_type")
    private String speedLimitType;
    @JsonProperty("speed_limit_fixed_up")
    private int speedLimitFixedUp;
    @JsonProperty("speed_limit_fixed_down")
    private int speedLimitFixedDown;
    @JsonProperty("burst_type")
    private String burstType;
    @JsonProperty("aggregation")
    private int aggregation;
    @JsonProperty("burst_limit")
    private int burstLimit;
    @JsonProperty("burst_limit_fixed_up")
    private int burstLimitFixedUp;
    @JsonProperty("burst_limit_fixed_down")
    private int burstLimitFixedDown;
    @JsonProperty("burst_threshold")
    private int burstThreshold;
    @JsonProperty("burst_time")
    private int burstTime;
    @JsonProperty("burst_threshold_fixed_up")
    private int burstThresholdFixedUp;
    @JsonProperty("burst_threshold_fixed_down")
    private int burstThresholdFixedDown;
    private String priority;
    private String services;
    @JsonProperty("services_online")
    private String servicesOnline;
    @JsonProperty("with_vat")
    private boolean withVat;

    public boolean isWithVat() {
        return withVat;
    }

    public void setWithVat(String withVat) {
        if (StringUtils.isNotBlank(withVat)) {
            this.withVat = withVat.equalsIgnoreCase("1");
        }
    }

    public boolean isAvailableForServices() {
        return availableForServices;
    }

    public void setAvailableForServices(String availableForServices) {
        if (StringUtils.isNotBlank(availableForServices)) {
            this.availableForServices = availableForServices.equalsIgnoreCase("1");
        }
    }

    public boolean isUpdateServicePrice() {
        return updateServicePrice;
    }

    public void setUpdateServicePrice(String updateServicePrice) {
        if (StringUtils.isNotBlank(updateServicePrice)) {
            this.updateServicePrice = updateServicePrice.equalsIgnoreCase("1");
        }
    }

    public boolean isUpdateServiceName() {
        return updateServiceName;
    }

    public void setUpdateServiceName(String updateServiceName) {
        if (StringUtils.isNotBlank(updateServiceName)) {
            this.updateServiceName = updateServiceName.equalsIgnoreCase("1");
        }
    }

    public boolean isForceUpdateServicePrice() {
        return forceUpdateServicePrice;
    }

    public void setForceUpdateServicePrice(String forceUpdateServicePrice) {
        if (StringUtils.isNotBlank(forceUpdateServicePrice)) {
            this.forceUpdateServicePrice = forceUpdateServicePrice.equalsIgnoreCase("1");
        }
    }

    public boolean isForceUpdateServiceName() {
        return forceUpdateServiceName;
    }

    public void setForceUpdateServiceName(String forceUpdateServiceName) {
        if (StringUtils.isNotBlank(forceUpdateServiceName)) {
            this.forceUpdateServiceName = forceUpdateServiceName.equalsIgnoreCase("1");
        }
    }

    @JsonProperty("available_for_services")
    private boolean availableForServices;
    @JsonProperty("update_service_price")
    private boolean updateServicePrice;
    @JsonProperty("update_service_name")
    private boolean updateServiceName;
    @JsonProperty("force_update_service_price")
    private boolean forceUpdateServicePrice;
    @JsonProperty("force_update_service_name")
    private boolean forceUpdateServiceName;
    private List<String> billingTypes;
    @JsonProperty("billing_days_count")
    private int billingDaysCount;
    @JsonProperty("custom_period")
    private int customPeriod;

    @JsonProperty("transaction_category_id")
    private int transactionCategoryId;

    @JsonProperty("updated_at")
    @JsonFormat(pattern="yyyy-MM-dd hh:mm:ss")
    private Date updatedAt;
    @JsonProperty("additional_attributes")
    private AdditionalAttributes additionalAttributes;

    public SplynxPlanResponse(Plan plan) {

    }

//    public SplynxPlanResponse(PlansEntity plansEntity) {
//        this.services = plansEntity.getActions();
//        this.id = plansEntity.getId();
//        this.serviceName = plansEntity.getConnectivityProvider();
//        this. plansEntity.getInstallOptions();
//        plansEntity.getPrice();
//        plansEntity.getShortcode();
//        plansEntity.getServiceType();
//        this.title = plansEntity.getTitle();
//        plansEntity.getOnlyPromo();
//        plansEntity.getServiceDetails();
//    }


}
