package za.co.ideal.ca.adminportal.dto.responses;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import za.co.ideal.ca.adminportal.entities.PromotionsEntity;
import za.co.ideal.ca.adminportal.models.Promotion;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PromoPlanPagingResponse {

    private long id;
    private String promoCode;
    private String internetPlan;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateCreated;
    private String status;

    public PromoPlanPagingResponse(Promotion promotionsEntity) {
        this.id = promotionsEntity.getId(); // Confirm this is promo id and not plan id
        this.promoCode = promotionsEntity.getPromoCode();
        this.internetPlan = promotionsEntity.getPlan().getTitle();
        this.dateCreated = promotionsEntity.getPlan().getDateCreated();
        this.status = promotionsEntity.getPlan().getStatus();

    }

}
