package za.co.ideal.ca.adminportal.services;

import za.co.ideal.ca.adminportal.dto.Login;
import za.co.ideal.ca.adminportal.dto.responses.AuthResponse;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public interface AuthService {

//    void login(Login login) throws IOException, RadiusException;

    AuthResponse getAuthToken() throws NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException;

}
