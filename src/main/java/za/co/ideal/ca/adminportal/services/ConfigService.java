package za.co.ideal.ca.adminportal.services;

import za.co.ideal.ca.adminportal.dto.responses.ConfigData;

import javax.servlet.http.HttpServletRequest;

public interface ConfigService {

    ConfigData retrieveConfigData(HttpServletRequest request);

}
