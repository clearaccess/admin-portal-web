package za.co.ideal.ca.adminportal.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import za.co.ideal.ca.adminportal.dto.UserRole;
import za.co.ideal.ca.adminportal.dto.responses.ConfigData;
import za.co.ideal.ca.adminportal.dto.responses.SplynxPlanResponse;
import za.co.ideal.ca.adminportal.entities.UserRolesEntity;
import za.co.ideal.ca.adminportal.repositories.UserRolesRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ConfigServiceImpl implements ConfigService {

    private final JdbcTemplate jdbcTemplate;
    private final PlanService planService;
    private final UserRolesRepository userRolesRepository;

    private static final Logger logger = LogManager.getLogger(ConfigServiceImpl.class);

    public ConfigServiceImpl(JdbcTemplate jdbcTemplate, PlanService planService, UserRolesRepository userRolesRepository) {
        this.jdbcTemplate = jdbcTemplate;
        this.planService = planService;
        this.userRolesRepository = userRolesRepository;
    }

    @Override
    public ConfigData retrieveConfigData(HttpServletRequest request) {
        List<String> promoTypes = new ArrayList<>();
        List<String> serviceTypes;
        List<String> serviceProviders;

        // --- Retrieve Promo Types ---

        String sql = "select distinct promo_type FROM clearaccess_staging.promotions";

        ParameterizedTypeReference<List<String>> responseType = new ParameterizedTypeReference<List<String>>() {};
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);

        for (Map row: rows) {
            String promoType = (String) row.get("promo_type");
            promoTypes.add(promoType);
        }

        // --- Retrieve service types and service providers ---

        List<SplynxPlanResponse> splynxPlanResponses = planService.findAllPlans(request);

        serviceTypes = splynxPlanResponses
                .stream()
                .map(splynxPlanResponse -> splynxPlanResponse.getAdditionalAttributes().getServiceType())
                .distinct()
                .collect(Collectors.toList());

        serviceProviders = splynxPlanResponses
                .stream()
                .map(splynxPlanResponse -> splynxPlanResponse.getAdditionalAttributes().getConnectivityProvider())
                .distinct()
                .collect(Collectors.toList());

        List<UserRole> userRoles = userRolesRepository
                .findAll()
                .stream()
                .map(UserRole::new)
                .collect(Collectors.toList());

        ConfigData configData = new ConfigData(promoTypes, serviceTypes, serviceProviders, userRoles);

        logger.info("Config data | " + configData.toString());

        return configData;

    }
}














