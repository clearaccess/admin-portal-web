package za.co.ideal.ca.adminportal.entities;

import lombok.*;

import javax.persistence.*;
import java.util.Collection;

@Data
@Entity
@NoArgsConstructor
@Table(name = "portal_user", schema = "clearaccess_staging", catalog = "clearaccess")
public class PortalUserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    @Basic@Column(name = "username")
    private String username;
    @Basic@Column(name = "password")
    private String password;
//    @OneToMany(mappedBy = "portalUserByUserId")
//    private Collection<UserRoleEntity> userRolesById;

    public PortalUserEntity(String username, String password) {
        this.username = username;
        this.password = password;
    }

}
