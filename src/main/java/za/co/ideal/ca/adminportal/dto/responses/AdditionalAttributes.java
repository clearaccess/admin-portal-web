package za.co.ideal.ca.adminportal.dto.responses;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdditionalAttributes {

    @JsonProperty("28e_shortcode")
    private String twentyEightEastShortCode;
    @JsonProperty("connectivity_provider")
    private String connectivityProvider;
    @JsonProperty("gl_code")
    private String glCode;
    @JsonProperty("install_options")
    private List<String> installOptions;
    @JsonProperty("service_description")
    private String serviceDescription;
    @JsonProperty("service_details")
    private String serviceDetails;
    @JsonProperty("service_level")
    private String serviceLevel;
    @JsonProperty("service_type")
    private String serviceType;

}
