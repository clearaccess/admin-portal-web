package za.co.ideal.ca.adminportal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableJpaRepositories("za.co.ideal.ca.adminportal.entities.*")
//@ComponentScan(basePackages = { "za.co.ideal.ca.adminportal.entities.*" })
//@EntityScan("za.co.ideal.ca.adminportal.entities.*")
public class AdminPortalApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdminPortalApplication.class, args);
    }

}
