package za.co.ideal.ca.adminportal.dto.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FilterPlans {

    private List<String> serviceTypes;
    private List<String> serviceProviders;

}
