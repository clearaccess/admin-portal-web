package za.co.ideal.ca.adminportal.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import za.co.ideal.ca.adminportal.dto.responses.Plan;
import za.co.ideal.ca.adminportal.entities.PromotionsEntity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Id;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Promotion {
    private long id;
    private Double price;
    private String promoCode;
    private String promoOffer;
    private String promoType;
    private List<Plan> plans;
    private Plan plan;

    public Promotion(PromotionsEntity promotionsEntity, Plan plan) {
        this.id = promotionsEntity.getId();
        this.price = promotionsEntity.getPrice();
        this.promoCode = promotionsEntity.getPromoCode();
        this.promoOffer = promotionsEntity.getPromoOffer();
        this.promoType = promotionsEntity.getPromoType();
        this.plan = plan;
    }

    public Promotion(PromotionsEntity promotionsEntity, List<Plan> plans) {
        this.id = promotionsEntity.getId();
        this.price = promotionsEntity.getPrice();
        this.promoCode = promotionsEntity.getPromoCode();
        this.promoOffer = promotionsEntity.getPromoOffer();
        this.promoType = promotionsEntity.getPromoType();
        this.plans = plans;
    }

    public Promotion(PromotionsEntity promotionsEntity) {
        this.id = promotionsEntity.getId();
        this.price = promotionsEntity.getPrice();
        this.promoCode = promotionsEntity.getPromoCode();
        this.promoOffer = promotionsEntity.getPromoOffer();
        this.promoType = promotionsEntity.getPromoType();
    }

}
