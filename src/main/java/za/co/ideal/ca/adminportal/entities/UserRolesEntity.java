package za.co.ideal.ca.adminportal.entities;

import lombok.*;

import javax.persistence.*;
import java.util.Collection;

@Data
@Entity
@NoArgsConstructor
@Table(name = "user_roles", schema = "clearaccess_staging", catalog = "clearaccess")
public class UserRolesEntity {
    @Id@Column(name = "id")
    private long id;
    @Basic@Column(name = "role")
    private String role;
//    @OneToMany(mappedBy = "userRolesByUserRolesId")
//    private Collection<UserRoleEntity> userRolesById;

}
