package za.co.ideal.ca.adminportal.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import za.co.ideal.ca.adminportal.entities.UserRolesEntity;

import java.util.Optional;

public interface UserRolesRepository extends JpaRepository<UserRolesEntity, Long> {
    Optional<UserRolesEntity> findByRole(String role);
}
