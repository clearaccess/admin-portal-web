package za.co.ideal.ca.adminportal.dto.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AuthResponse {
    private String access_token;
    private long access_token_expiration;
    private String refresh_token;
    private long refresh_token_expiration;
}
