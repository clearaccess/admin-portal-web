package za.co.ideal.ca.adminportal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import za.co.ideal.ca.adminportal.entities.PortalUserEntity;
import za.co.ideal.ca.adminportal.entities.UserRoleEntity;

import java.util.List;
import java.util.Optional;

public interface UserRoleRepository extends JpaRepository<UserRoleEntity, Long> {

    Optional<List<UserRoleEntity>> findAllByPortalUserByUserId(PortalUserEntity portalUserEntity);
    void deleteAllByPortalUserByUserId(PortalUserEntity portalUserEntity);

}
