package za.co.ideal.ca.adminportal.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import za.co.ideal.ca.adminportal.dto.requests.PostPromotion;
import za.co.ideal.ca.adminportal.dto.responses.Plan;
import za.co.ideal.ca.adminportal.entities.PromotionsEntity;
import za.co.ideal.ca.adminportal.exceptions.GenericException;
import za.co.ideal.ca.adminportal.models.Promotion;
import za.co.ideal.ca.adminportal.repositories.PromotionsRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PromotionServiceImpl implements PromotionService {

    private static final Logger logger = LogManager.getLogger(PromotionServiceImpl.class);

    private final PromotionsRepository promotionsRepository;
    private final PlanService planService;

    public PromotionServiceImpl(PromotionsRepository promotionsRepository, PlanService planService) {
        this.promotionsRepository = promotionsRepository;
        this.planService = planService;
    }

    @Override
    public Promotion findById(HttpServletRequest request, long id) {
        PromotionsEntity promotions = promotionsRepository.findById(id)
                .orElseThrow(() -> new GenericException("Could not find promotion with id " + id, HttpStatus.NOT_FOUND, "Could not find promotion with id " + id));

        List<PromotionsEntity> promotionsEntities = promotionsRepository.findAllByPromoCodeAndPromoOfferAndPromoType(promotions.getPromoCode(), promotions.getPromoOffer(), promotions.getPromoType());

        List<Plan> plans = planService.getPlansForPromo(request, promotionsEntities);

        Promotion promotion = new Promotion(promotions, plans);
        return promotion;
    }

    @Override
    public Page<Promotion> findAll(int page, int limit) {
        Pageable pageable = PageRequest.of(page, limit);

        Page<PromotionsEntity> promotionsEntities = promotionsRepository.findAll(pageable);
        List<Promotion> promotions = promotionsEntities
                .stream()
                .map(Promotion::new)
                .collect(Collectors.toList());

        Page<Promotion> promotionsPage = new PageImpl<Promotion>(promotions, pageable, promotionsEntities.getTotalElements());

        return promotionsPage;
    }

    @Override
    public Promotion savePromotion(HttpServletRequest request, PostPromotion postPromotion) {
        // Save promotion
        PromotionsEntity promotion = new PromotionsEntity(postPromotion);

        postPromotion.getPlanIds()
                .forEach(aLong -> {
                    PromotionsEntity promotions = new PromotionsEntity();
                    promotions.setPromoCode(postPromotion.getPromoCode());
                    promotions.setPromoOffer(postPromotion.getPromoOffer());
                    promotions.setPromoType(postPromotion.getPromoType());
                    promotions.setPrice(postPromotion.getPrice());
                    promotions.setPlanId(aLong);
                    promotions.setId(promotionsRepository.count() + 1);
                    promotionsRepository.save(promotions);
                });

        return null;

//        PromotionsEntity savedPromotion =
        // Apply promotion to plans
        // How do we apply a promotion to a plan in splynx?
        // The plan_id column is linked to the Splynx service id
        // So plan_id === splynx_service_id
        // take splynx service_id -> insert into promotions table (for all services)
    }

    @Override
    public List<PromotionsEntity> getPromosForPlan(List<Plan> plans, int page, int limit) {

        Pageable pageable = PageRequest.of(page, limit);

        List<PromotionsEntity> promotionsEntities = promotionsRepository.findAllByPlanIdIn(
                plans.
                        stream()
                        .map(Plan::getId)
                        .collect(Collectors.toList()),
                pageable
        )
                .stream()
                .sorted(Comparator.comparingLong(PromotionsEntity::getId))
                .collect(Collectors.toList());

        logger.info("Promotion Entities size ! " + promotionsRepository.findAll().size());

        return promotionsEntities;

    }

    @Override
    public void deletePromotion(long id) {

        PromotionsEntity promotions = promotionsRepository.findById(id)
                .orElseThrow(() -> new GenericException("Could not find promotion with id " + id, HttpStatus.NOT_FOUND, "Could not find promotion with id " + id));

        List<PromotionsEntity> promotionsEntities = promotionsRepository.findAllByPromoCodeAndPromoOfferAndPromoType(promotions.getPromoCode(), promotions.getPromoOffer(), promotions.getPromoType());
        promotionsRepository.deleteAll(promotionsEntities);
    }

    @Override
    public void updatePromotion(PostPromotion postPromotion, long id) {

        List<PromotionsEntity> promotions = promotionsRepository.findAllByPlanIdIn(postPromotion.getPlanIds())
                .orElseThrow(() -> new GenericException("Could not find promotion with id " + id, HttpStatus.NOT_FOUND, "Could not find promotion with id " + id));

        promotions
                .forEach(promotionsEntity -> {

                    // If one exists with the plan id already, edit the properties
                    List<Long> sharedPlanIds = postPromotion
                            .getPlanIds()
                            .stream()
                            .filter(aLong -> aLong == promotionsEntity.getPlanId())
                            .collect(Collectors.toList());

                    sharedPlanIds
                            .forEach(aLong -> {
                                PromotionsEntity promotions1 = promotionsRepository.findByIdAndPlanId(promotionsEntity.getId(), aLong);
                                promotions1.setPromoType(postPromotion.getPromoType());
                                promotions1.setPrice(postPromotion.getPrice());
                                promotions1.setPromoOffer(postPromotion.getPromoOffer());
                                promotions1.setPromoCode(postPromotion.getPromoCode());
                                promotionsRepository.save(promotions1);
                            });

                    // if one doesn't, create new entity with it in as well with all the other properties
                    List<Long> unsharedPlanIds = postPromotion
                            .getPlanIds()
                            .stream()
                            .filter(aLong -> aLong != promotionsEntity.getPlanId())
                            .collect(Collectors.toList());

                    unsharedPlanIds
                            .forEach(aLong -> {
                                PromotionsEntity promotions1 = new PromotionsEntity();
                                promotions1.setPromoType(postPromotion.getPromoType());
                                promotions1.setPrice(postPromotion.getPrice());
                                promotions1.setPromoOffer(postPromotion.getPromoOffer());
                                promotions1.setPromoCode(postPromotion.getPromoCode());
                                promotions1.setPlanId(aLong);
                                promotions1.setId(promotionsRepository.count() + 1);
                                promotionsRepository.save(promotions1);
                            });

                });

        // do we need to update splynx?
    }


}
