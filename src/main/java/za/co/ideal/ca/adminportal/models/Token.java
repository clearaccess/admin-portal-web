package za.co.ideal.ca.adminportal.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import za.co.ideal.ca.adminportal.dto.UserRole;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Token {

    private String access_token;
    private String userId;
    private List<UserRole> userRoles;

}
