package za.co.ideal.ca.adminportal.controllers;

import lombok.NonNull;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import za.co.ideal.ca.adminportal.dto.requests.PostPromotion;
import za.co.ideal.ca.adminportal.dto.responses.Plan;
import za.co.ideal.ca.adminportal.dto.responses.PromoPlanPagingResponse;
import za.co.ideal.ca.adminportal.dto.responses.SplynxPlanResponse;
import za.co.ideal.ca.adminportal.entities.PromotionsEntity;
import za.co.ideal.ca.adminportal.exceptions.GenericException;
import za.co.ideal.ca.adminportal.models.Promotion;
import za.co.ideal.ca.adminportal.services.PlanService;
import za.co.ideal.ca.adminportal.services.PromotionService;
import za.co.ideal.ca.adminportal.services.SessionService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping(value = "/ca-portal/api/v1/promotion")
public class PromotionController {

    private final PromotionService promotionService;
    private final PlanService planService;
    private final SessionService sessionService;
    private static final Logger logger = LogManager.getLogger(PromotionController.class);

    public PromotionController(PromotionService promotionService, PlanService planService, SessionService sessionService) {
        this.promotionService = promotionService;
        this.planService = planService;
        this.sessionService = sessionService;
    }

    @GetMapping("{id}")
    public ResponseEntity<Promotion> findById(HttpServletRequest request, @PathVariable long id) {

        if (!sessionService.isTokenValid(request)) {
            throw new GenericException("Token is invalid", HttpStatus.FORBIDDEN, "Token is invalid");
        }

        return ResponseEntity.ok(promotionService.findById(request, id));

    }

//    @GetMapping("{page}/{limit}")
//    public ResponseEntity<Page<Promotion>> findAll(@PathVariable int page, @PathVariable int limit) {
//        return ResponseEntity.ok(promotionService.findAll(page, limit));
//
//    }

    @GetMapping("/{page}/{limit}")
    public ResponseEntity<Page<PromoPlanPagingResponse>> findAllPlans(HttpServletRequest request, @NonNull @PathVariable int page, @NonNull @PathVariable int limit) {

        if (!sessionService.isTokenValid(request)) {
            throw new GenericException("Token is invalid", HttpStatus.FORBIDDEN, "Token is invalid");
        }

        try {
            List<SplynxPlanResponse> plans = planService.findAllPlans(request);
            List<Plan> plans1 = plans
                    .stream()
                    .map(Plan::new)
                    .collect(Collectors.toList());

            List<PromotionsEntity> promotionsEntities = promotionService.getPromosForPlan(plans1, page, limit);

            List<Promotion> promotions = promotionsEntities
                    .stream()
                    .map(promotionsEntity -> {
                        logger.info("PLAN IDDDDDD | " + promotionsEntity.getPlanId());
                        logger.info("PLANNNNN | ");
                        return new Promotion(promotionsEntity, plans1.stream().filter(plan -> plan.getId() == promotionsEntity.getPlanId()).collect(Collectors.toList()).get(0));
                    })
                    .collect(Collectors.toList());

            return ResponseEntity.ok(planService.mapPromoPlans(promotions, page, limit, plans1));
        } catch (Exception e) {
            throw new GenericException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }

//        List<SplynxPlanResponse> = new
    }

    @PostMapping
    public ResponseEntity<Promotion> savePromotion(HttpServletRequest request, @RequestBody @Validated PostPromotion postPromotion) {

        if (!sessionService.isTokenValid(request)) {
            throw new GenericException("Token is invalid", HttpStatus.FORBIDDEN, "Token is invalid");
        }

        return ResponseEntity.ok(promotionService.savePromotion(request, postPromotion));
    }

    @PutMapping("/{id}")
    public void updatePromotion(@RequestBody @Validated PostPromotion postPromotion, @PathVariable long id, HttpServletRequest request) {

        if (!sessionService.isTokenValid(request)) {
            throw new GenericException("Token is invalid", HttpStatus.FORBIDDEN, "Token is invalid");
        }

        promotionService.updatePromotion(postPromotion, id);
    }

    @DeleteMapping("/{id}")
    public void deletePromotion(@PathVariable long id, HttpServletRequest request) {
        if (!sessionService.isTokenValid(request)) {
            throw new GenericException("Token is invalid", HttpStatus.FORBIDDEN, "Token is invalid");
        }
        promotionService.deletePromotion(id);
    }

}
