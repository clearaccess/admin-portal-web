//package za.co.ideal.ca.adminportal.services;
//
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.mail.SimpleMailMessage;
//import org.springframework.mail.javamail.JavaMailSender;
//import org.springframework.stereotype.Service;
//
//@Service
//public class EmailService {
//
//    private final JavaMailSender javaMailSender;
//
//    @Value("{application.properties.app.url}")
//    private String appUrl;
//
//    public EmailService(JavaMailSender javaMailSender) {
//        this.javaMailSender = javaMailSender;
//    }
//
//    public void sendSimpleMessage(String to, String subject, String text) {
//        SimpleMailMessage message = new SimpleMailMessage();
//        message.setFrom("noreply@clearaccess.co.za");
//        message.setTo(to);
//        message.setSubject(subject);
//        message.setText(text);
//        javaMailSender.send(message);
//    }
//
//    @RabbitListener(queues = "sendUserRegistrationMail")
//    public void listen(String username, String uuid) {
//        System.out.println("Message read from sendUserRegistrationMail : " + username);
//        this.sendSimpleMessage(username, "Account registration", String.format("Please copy the following text into your browser to create your account %s", appUrl + "/verify-user/" + uuid));
//    }
//
//}
