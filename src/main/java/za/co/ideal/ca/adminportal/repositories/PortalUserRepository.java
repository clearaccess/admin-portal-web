package za.co.ideal.ca.adminportal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import za.co.ideal.ca.adminportal.entities.PortalUserEntity;

import java.util.Optional;

public interface PortalUserRepository extends JpaRepository<PortalUserEntity, Long>, PagingAndSortingRepository<PortalUserEntity, Long> {
    Optional<PortalUserEntity> findByUsername(String username);
}
