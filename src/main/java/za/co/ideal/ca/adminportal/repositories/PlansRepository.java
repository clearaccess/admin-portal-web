package za.co.ideal.ca.adminportal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import za.co.ideal.ca.adminportal.entities.PlansEntity;

public interface PlansRepository extends JpaRepository<PlansEntity, Long> {
}
