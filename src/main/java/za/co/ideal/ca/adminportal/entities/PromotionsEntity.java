package za.co.ideal.ca.adminportal.entities;

import lombok.*;
import za.co.ideal.ca.adminportal.dto.requests.PostPromotion;

import javax.persistence.*;
import java.util.Objects;

@Data
@Entity
@NoArgsConstructor
@Table(name = "promotions", schema = "clearaccess_staging", catalog = "clearaccess")
public class PromotionsEntity {
    @Id
    @Column(name = "id", nullable = false)
    private long id;
    @Basic@Column(name = "price")
    private Double price;
    @Basic@Column(name = "promo_code", nullable = false)
    private String promoCode;
    @Basic@Column(name = "promo_offer", nullable = false)
    private String promoOffer;
    @Basic@Column(name = "promo_type", nullable = false)
    private String promoType;
//    @ManyToOne@JoinColumn(name = "plan_id", referencedColumnName = "id", nullable = false)
//    private PlansEntity plansByPlanId;

    @Column(name = "plan_id", nullable = false)
    private long planId;

    public PromotionsEntity(PostPromotion postPromotion) {
        this.price = postPromotion.getPrice();
        this.promoCode = postPromotion.getPromoCode();
        this.promoOffer = postPromotion.getPromoOffer();
        this.promoType = postPromotion.getPromoType();
    }
}
