package za.co.ideal.ca.adminportal.dto.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import za.co.ideal.ca.adminportal.dto.responses.Plan;
import za.co.ideal.ca.adminportal.entities.PromotionsEntity;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostPromotion {

    private long id;
    private Double price;
    private String promoCode;
    private String promoOffer;
    private String promoType;
    private List<Long> planIds;

}
