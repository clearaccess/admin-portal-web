//package za.co.ideal.ca.adminportal.authentication;
//
//import com.auth0.jwt.JWT;
//import com.auth0.jwt.algorithms.Algorithm;
//import io.jsonwebtoken.JwtBuilder;
//import io.jsonwebtoken.Jwts;
//import io.jsonwebtoken.SignatureAlgorithm;
//import lombok.extern.log4j.Log4j2;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.http.HttpStatus;
//import org.springframework.security.authentication.AuthenticationProvider;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.web.context.request.RequestContextHolder;
//import org.springframework.web.context.request.ServletRequestAttributes;
//import org.tinyradius.packet.RadiusPacket;
//import za.co.ideal.ca.adminportal.config.JwtSession;
//import za.co.ideal.ca.adminportal.config.yaml.RadiusYamlProperties;
//import za.co.ideal.ca.adminportal.dto.UserRole;
//import za.co.ideal.ca.adminportal.entities.PortalUserEntity;
//import za.co.ideal.ca.adminportal.entities.UserRoleEntity;
//import za.co.ideal.ca.adminportal.exceptions.GenericException;
//import za.co.ideal.ca.adminportal.models.Token;
//import za.co.ideal.ca.adminportal.repositories.PortalUserRepository;
//import za.co.ideal.ca.adminportal.repositories.UserRoleRepository;
//
//import javax.annotation.PostConstruct;
//import javax.crypto.spec.SecretKeySpec;
//import javax.servlet.http.HttpSession;
//import javax.xml.bind.DatatypeConverter;
//import java.nio.charset.StandardCharsets;
//import java.security.Key;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//import java.util.stream.Collectors;
//
//@Log4j2
//public class RadiusAuthenticationProvider implements AuthenticationProvider {
//
//    @Value("${application.properties.radius.server}")
//    private String serverConfigurationToken;
//
//    private List<NetworkAccessServer> clients = new ArrayList<>();
//
//    @Autowired
//    private PortalUserRepository portalUserRepository;
//
//    @Autowired
//    private UserRoleRepository userRoleRepository;
//
//    @PostConstruct
//    public void initServers() {
//        System.out.println("INIT SERVERS!!!!!!!!!!!!!");
//        List<RadiusServer> servers = RadiusUtil.parseServerConfigurationToken(serverConfigurationToken);
//        log.info("SERVERS | " + servers.size());
//        servers.forEach(it -> {
//            log.info("SERVER | " + it.getIp());
//            clients.add(new NetworkAccessServer(it));
//        });
//    }
//
//    @Override
//    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
//        String username = authentication.getName();
//        RadiusPacket response = null;
//        int attemptCount = 0;
//        log.warn(String.format("User calling radius with username %s and password %s", username, authentication.getCredentials()));
//        log.warn(String.format("Attempt count: %s and client size %s", attemptCount, clients.size()));
//        while (response == null && attemptCount++ < clients.size()) {
//            response = authenticateInternally(clients.get(attemptCount - 1), username,
//                    authentication.getCredentials().toString());
//        }
//        if (response == null) {
//            log.warn("User {}, calling radius does not return any value.", username);
//            return null;
//        }
//        if (response.getPacketType() == RadiusPacket.ACCESS_ACCEPT) {
//            log.info("User {} successfully authenticated using radius", username);
//            return generateSessionFromSuccessfulLogin(username);
//        } else {
//            log.warn("User {}, returned response {}", username, response);
//            return null;
//        }
//    }
//
//    private RadiusPacket authenticateInternally(NetworkAccessServer client, String username, String password) {
//        log.warn("Calling radius server to authenticate user {}", username);
//        try {
//            return client.authenticate(username, password);
//        } catch (Exception e) {
//            log.warn("Exception when calling remote radius server.", e);
//            return null;
//        }
//    }
//
//    @Override
//    public boolean supports(Class<?> authentication) {
//        return authentication.equals(UsernamePasswordAuthenticationToken.class);
//    }
//
//    public static HttpSession getHttpSession() {
//        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
//        return attr.getRequest().getSession(true);
//    }
//
//    private UsernamePasswordAuthenticationToken generateSessionFromSuccessfulLogin(String username) {
//        JwtSession jwtSession = new JwtSession();
//
//        // Get user from user name
//        PortalUserEntity portalUserEntity = portalUserRepository.findByUsername(username)
//                .orElseThrow(() -> new GenericException(String.format("no user found with username %s"), HttpStatus.NOT_FOUND, String.format("no user found with username %s")));
//        // Generate jwt token based on user, roles]
//        List<UserRoleEntity> userRoleEntities = userRoleRepository.findAllByPortalUserByUserId(portalUserEntity)
//                .orElseThrow(() -> new GenericException(String.format("no roles found for user with username %s"), HttpStatus.NOT_FOUND, String.format("no roles found for user with username %s")));
//
//        List<UserRole> userRoles = userRoleEntities.stream().map(UserRole::new).collect(Collectors.toList());
//        List<String> userRoleStringList = userRoles.stream().map(UserRole::getRole).collect(Collectors.toList());
//        String[] userRoleListString = userRoleStringList.toArray(new String[0]);
//
//        String jwtToken = createJWT(getHttpSession().getId(), "clearaccess", (60*60*1000), userRoleListString);
//        // Generate session
//        // Verify session is valid on each call
//        // Decrypt token to check for roles
//        jwtSession.setToken(new Token(jwtToken, String.valueOf(portalUserEntity.getId()), userRoles));
//        HttpSession httpSession = getHttpSession();
//        httpSession.setAttribute("token", jwtSession);
//
//        log.info("Successfully linked radius user with db user.");
//
//        return new UsernamePasswordAuthenticationToken(username, "", new ArrayList<>());
//    }
//
//    public static String createJWT(String id, String issuer, long ttlMillis, String[] userRoleList) {
//        Algorithm algorithm = Algorithm.HMAC256("testing123".getBytes(StandardCharsets.UTF_8));
//        long nowMillis = System.currentTimeMillis();
//        Date now = new Date(nowMillis);
//
//        String token = JWT.create()
//                .withIssuer(issuer)
//                .withJWTId(id)
//                .withIssuedAt(now)
//                .withArrayClaim("roles", userRoleList)
//                .withClaim("exp", now.toInstant().getEpochSecond() + ttlMillis)
//                .withExpiresAt(new Date(nowMillis + ttlMillis))
//                .sign(algorithm);
//
//        return token;
//    }
//
//}
