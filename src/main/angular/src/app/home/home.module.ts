import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { SharedModule } from '../shared/shared.module';
import {AuthApiClass} from "../api/auth.api.class";
import {AuthService} from "../services/auth/auth.service";
import {SessionService} from "../services/session/session.service";
import {SessionApiClass} from "../api/session.api.class";

@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    SharedModule
  ],
  providers: [
    AuthApiClass,
    AuthService,
    SessionService,
    SessionApiClass
  ]
})
export class HomeModule { }
