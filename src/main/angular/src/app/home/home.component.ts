import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from "../services/auth/auth.service";
import {NavMenuItem} from "../interfaces/route/NavMenuItem";
import {Route} from "../interfaces/route/Route";
import {Subscription} from "rxjs";
import {SessionService} from "../services/session/session.service";
import {SnackbarService} from "../services/snackbar/snackbar.service";
import {HttpErrorResponse} from "@angular/common/http";
import {ConfigDetail} from "../interfaces/config/ConfigDetail";
import {StaticService} from "../services/static/static.service";
import {ConfigService} from "../services/config/config.service";

export const staticRouteConfig = {
  promotionManager: {
    name: 'Promotion Manager',
    route: 'promotion-manager',
    code: 'PROMOTION_MANAGER',
    active: false
  }
};

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  svgImages = {
    promotionManager: `<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 21V5a2 2 0 00-2-2H7a2 2 0 00-2 2v16m14 0h2m-2 0h-5m-9 0H3m2 0h5M9 7h1m-1 4h1m4-4h1m-1 4h1m-5 10v-5a1 1 0 011-1h2a1 1 0 011 1v5m-4 0h4" /></svg>`
  };

  menuItems: Route[] = [];
  navMenuItems: NavMenuItem[] = [];
  menuItemsHovered: Route[] = [];
  activeNavMenuItem!: NavMenuItem | null;
  navMenuItemHovered: boolean = false;
  route!: string;
  // @Select(RouteState.route) $route!: Observable<string>;
  private routeObservable!: Subscription;

  private subscription1!: Subscription;
  private subscription2!: Subscription;

  // TODO: Get routes from JWT in java
  routeConfig: {allowedRoute: string}[] = [];
  loginDisplay = false;
  private subscription!: Subscription;
  public loading: boolean = true;

  constructor(
    private router: Router,
    private sessionService: SessionService,
    private snackBarService: SnackbarService,
    private configService: ConfigService,
    public staticService: StaticService
  ) {
    console.log('Home component loaded...');
  }

  routeHome(): void {
    if (this.navMenuItems.length === 1) {
      this.router.navigate(['/home/promotion-manager/promo-list']);
    } else {
      this.router.navigate(['/home'])
    }
  }

  routerLink(): string[] {
    if (this.navMenuItems.length === 1) {
      return ['/home/promotion-manager/promo-list'];
    } else {
      return ['/home'];
    }
  }

  pushIntoRouteConfig(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.sessionService.getSessionRoutes()
        .subscribe({
          next: (resp: string[]) => {
            resp
              ?.forEach(value => {
                this.routeConfig.push({allowedRoute: value});
              });
              resolve(this.routeConfig);
          },
          error: (err: HttpErrorResponse) => {
            this.snackBarService.showSnackBar(err.error.message);
            reject(err);
          }
        });
    });
  }

  async constructingMenuItems() {
    await this.pushIntoRouteConfig()
      .then((resp: {allowedRoute: string}[]) => {
        console.log('Allowed routes from promise return', resp);
      })
      .catch((err: HttpErrorResponse) => {
        this.snackBarService.showSnackBar(err.error.message);
      })
    console.log('Await construct menu items');
    await this.constructMenuItems();
    if (this.navMenuItems.length === 1) {
      this.router.navigate(['/home/promotion-manager/promo-list'])
    }
  }

  ngOnDestroy(): void {
    this.subscription1?.unsubscribe();
    this.subscription2?.unsubscribe();
  }

  ngOnInit(): void {

    this.subscription = this.configService.getAllConfigData()
      .subscribe({
        next: (data: ConfigDetail) => {
          console.log(data);
          this.loading = false;
          this.snackBarService.showSnackBar('Config data retrieved');
          this.staticService.setBehaviourSubject(data);
        },
        error: (err) => this.snackBarService.showSnackBar('Could not retrieve config data', 'Please refresh')
      });

    this.constructingMenuItems();
  }

  constructMenuItems(): void {
    console.log('Constructing menu items...');
    this.navMenuItems = [];

    for (const elt of this.routeConfig) {
      console.log(elt);
      switch (elt.allowedRoute) {
        case 'PROMOTION_MANAGER': {
          console.log('PROMOTIONMANAGER');
            this.navMenuItems.push({
              name: 'Promotion Manager',
              code: 'PROMOTION_MANAGER',
              headerText: 'Promotion Manager',
              image: this.svgImages.promotionManager,
              route: 'promotion-manager/promo-list',
              active: true
            });
          break;
        }
      }
    }
    this.navMenuItems.sort((a, b) => {
      const nameA = a.code.toUpperCase();
      const nameB = b.code.toUpperCase();
      if (nameA > nameB) {
        return 1;
      }
      if (nameA < nameB) {
        return -1;
      }
      // names must be equal
      return 0;
    });
  }

  enterHoveredMode(navMenuItem: NavMenuItem): void {
    this.navMenuItemHovered = true;
    this.activeNavMenuItem = navMenuItem;
  }

  exitHoveredMode(): void {
    this.navMenuItemHovered = false;
    this.activeNavMenuItem = null;
    this.menuItemsHovered = [];
  }

}
