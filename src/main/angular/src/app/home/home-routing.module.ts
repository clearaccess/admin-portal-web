import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home.component';
import {AuthGuard} from "../guards/auth/auth.guard";

const routes: Routes = [
    {
      path: '',
      component: HomeComponent,
      children: [
        {
          path: 'promotion-manager',
          loadChildren: () => import('../modules/promotion-manager/promotion-manager.module').then(m => m.PromotionManagerModule),
          canLoad: [AuthGuard]
        }
      ]
    }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
