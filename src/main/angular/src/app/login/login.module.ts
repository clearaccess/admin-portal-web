import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import {SharedModule} from "../shared/shared.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {UtilityService} from "../services/utility/utility.service";
import {AuthService} from "../services/auth/auth.service";
import {AuthApiClass} from "../api/auth.api.class";


@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    LoginRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    UtilityService,
    AuthService,
    AuthApiClass
  ]
})
export class LoginModule { }
