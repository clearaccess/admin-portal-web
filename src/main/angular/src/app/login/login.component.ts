import { Component, OnInit } from '@angular/core';
import {Login} from "../interfaces/Login";
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../services/auth/auth.service";
import {Token} from "../interfaces/Token";
import {SnackbarService} from "../services/snackbar/snackbar.service";
import {HttpErrorResponse} from "@angular/common/http";
import {UtilityService} from "../services/utility/utility.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loading: boolean = false;
  private login!: Login;
  public hide: boolean = true;

  public loginForm!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private snackbarService: SnackbarService,
    public utilityService: UtilityService,
    private router: Router
  ) {
    this.createForm();
  }

  createForm(): void {
    this.loginForm = this.fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  submitLogin(): void {
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;

    this.login = {
      username: this.loginForm.controls.username?.value,
      password: this.loginForm.controls.password?.value,
    };

    this.authService.login(this.login)
      .subscribe({
        next: (token: Token) => {
          console.log(token);
          this.snackbarService.showSnackBar("User successfully logged in");
          this.loading = false;
          sessionStorage.setItem('session-token', token.access_token);
          this.router.navigate(['home']);
        },
        error: (err: HttpErrorResponse) => {
          this.snackbarService.showSnackBar(err.error.message);
          this.loading = false;
        }
      })

  }

  get username(): AbstractControl {
    return this.loginForm.controls['username'];
  }

  get password(): AbstractControl {
    return this.loginForm.controls['password'];
  }

  ngOnInit(): void {
  }

  onSubmitForm($event: KeyboardEvent) {
    if ($event.key === 'Enter' && this.loginForm.valid) {
      this.submitLogin();
    }
  }

  onKeyDown($event: KeyboardEvent) {
    if ($event.key === 'Enter') {
      $event.preventDefault();
    }
  }
}
