import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationStart, Router} from "@angular/router";
import {filter} from "rxjs/operators";

@Component({
  selector: 'app-promotion-manager',
  templateUrl: './promotion-manager.component.html',
  styleUrls: ['./promotion-manager.component.scss']
})
export class PromotionManagerComponent implements OnInit, AfterViewInit {

  private navigationTrigger!: "imperative" | "popstate" | "hashchange" | undefined;

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) {
    router.events
    .pipe(filter(event => event instanceof NavigationStart))
    // @ts-ignore
    .subscribe((event: NavigationStart) => {
      console.log(event);
      this.navigationTrigger = event.navigationTrigger;
    });
  }

  ngOnInit(): void {
    console.log('Promotion manager loaded ngoninit');

  }

  ngAfterViewInit() {

    // if (this.navigationTrigger === 'imperative') {
    //   this.router.navigate(['./promo-list'], { relativeTo: this.route });
    // }
  }

}
