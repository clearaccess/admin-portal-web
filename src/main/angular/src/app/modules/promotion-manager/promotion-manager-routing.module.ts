import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PromotionManagerComponent } from './promotion-manager.component';
import {AuthGuard} from "../../guards/auth/auth.guard";

const routes: Routes = [
    {
      path: '',
      component: PromotionManagerComponent,
      children: [
        {
          path: 'promo/:id',
          loadChildren: () => import('./promo-detail/promo-detail.module').then(m => m.PromoDetailModule),
          // canLoad: [AuthGuard]
        },
        {
          path: 'promo',
          loadChildren: () => import('./promo-detail/promo-detail.module').then(m => m.PromoDetailModule),
          // canLoad: [AuthGuard]
        },
        {
          path: 'promo-list',
          loadChildren: () => import('./promo-list/promo-list.module').then(m => m.PromoListModule),
          // canLoad: [AuthGuard]
        }
      ]
    },
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PromotionManagerRoutingModule { }
