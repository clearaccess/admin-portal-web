import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PromoDetailComponent } from './promo-detail.component';

const routes: Routes = [{ path: '', component: PromoDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PromoDetailRoutingModule { }
