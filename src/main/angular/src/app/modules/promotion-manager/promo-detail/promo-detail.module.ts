import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PromoDetailRoutingModule } from './promo-detail-routing.module';
import { PromoDetailComponent } from './promo-detail.component';
import {SharedModule} from "../../../shared/shared.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {PromotionApiClass} from "../../../api/promotion.api.class";
import {PromoService} from "../../../services/promo/promo.service";
import {DialogService} from "../../../services/dialog/dialog.service";
import {MatDialogRef} from "@angular/material/dialog";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatChipsModule} from "@angular/material/chips";
import {PlanApiClass} from "../../../api/plan.api.class";
import {PlanService} from "../../../services/plan/plan.service";


@NgModule({
  declarations: [
    PromoDetailComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    PromoDetailRoutingModule,
    MatCheckboxModule,
    FormsModule,
    MatChipsModule
  ],
  providers: [
    PromotionApiClass,
    PromoService,
    PlanApiClass,
    PlanService,
    DialogService
  ]
})
export class PromoDetailModule { }
