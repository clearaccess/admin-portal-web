import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {SnackbarService} from "../../../services/snackbar/snackbar.service";
import {HttpErrorResponse} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {ConfigDetail} from "../../../interfaces/config/ConfigDetail";
import {Promotion} from "../../../interfaces/promotion/Promotion";
import {StaticService} from "../../../services/static/static.service";
import {UtilityService} from "../../../services/utility/utility.service";
import {PromoService} from "../../../services/promo/promo.service";
import {DialogService} from "../../../services/dialog/dialog.service";
import {PostPromotion} from "../../../interfaces/promotion/PostPromotion";
import {Plan} from "../../../interfaces/plan/Plan";
import { DialogData } from 'src/app/interfaces/generic/DialogData';
import {PlanService} from "../../../services/plan/plan.service";
import {PlanFilterOptions} from "../../../interfaces/filter/PlanFilterOptions";

export interface Filter {
  name: string;
  completed: boolean;
  color: string;
  subtasks?: Filter[];
}

@Component({
  selector: 'app-promo-detail',
  templateUrl: './promo-detail.component.html',
  styleUrls: ['./promo-detail.component.scss']
})
export class PromoDetailComponent implements OnInit, OnDestroy {

  paramSubscription: Subscription | undefined;

  promoForm!: FormGroup;
  loading: boolean = false;
  promoId!: string;
  postPromotion!: PostPromotion;
  promotion!: Promotion;

  keyIndividualDetail!: Promotion;
  public configData!: ConfigDetail;
  public subscription!: Subscription;
  private subscription2!: Subscription;
  private subscription3!: Subscription;
  private subscription4!: Subscription;
  private subscription5!: Subscription;
  private subscription6!: Subscription;
  private subscription7!: Subscription;

  public scrollToDocuments!: boolean;

  public plansPopulated: boolean = false;
  public plans!: Plan[];

  public filterOptions!: PlanFilterOptions;
  public filteredPlans!: Plan[];

  // checkbox
  serviceTypeFilter: Filter = {
    name: 'Service Type',
    completed: false,
    color: 'primary',
    subtasks: []
  };

  serviceProviderFilter: Filter = {
    name: 'Service Provider',
    completed: false,
    color: 'primary',
    subtasks: []
  };

  allCompleteServiceTypeFilter: boolean = false;
  allCompleteServiceProviderFilter: boolean = false;

  updateAllCompleteServiceTypeFilter() {
    this.allCompleteServiceTypeFilter = this.serviceTypeFilter.subtasks != null && this.serviceTypeFilter.subtasks.every(t => t.completed);
  }

  someCompleteServiceTypeFilter(): boolean {
    if (this.serviceTypeFilter.subtasks == null) {
      return false;
    }
    return this.serviceTypeFilter.subtasks.filter(t => t.completed).length > 0 && !this.allCompleteServiceTypeFilter;
  }

  setAllServiceTypeFilter(completed: boolean) {
    this.allCompleteServiceTypeFilter = completed;
    if (this.serviceTypeFilter.subtasks == null) {
      return;
    }
    this.serviceTypeFilter.subtasks.forEach(t => (t.completed = completed));
  }

  updateAllCompleteServiceProviderFilter() {
    this.allCompleteServiceTypeFilter = this.serviceProviderFilter.subtasks != null && this.serviceProviderFilter.subtasks.every(t => t.completed);
  }

  someCompleteServiceProviderFilter(): boolean {
    if (this.serviceProviderFilter.subtasks == null) {
      return false;
    }
    return this.serviceProviderFilter.subtasks.filter(t => t.completed).length > 0 && !this.allCompleteServiceProviderFilter;
  }

  setAllServiceProviderFilter(completed: boolean) {
    this.allCompleteServiceProviderFilter = completed;
    if (this.serviceProviderFilter.subtasks == null) {
      return;
    }
    this.serviceProviderFilter.subtasks.forEach(t => (t.completed = completed));
  }

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private staticService: StaticService,
    public utilityService: UtilityService,
    private promoService: PromoService,
    private snackBarService: SnackbarService,
    private dialogService: DialogService,
    private planService: PlanService
  ) {
    // this.staticService.loading.next(true);
    this.createForm();

    this.subscription = this.route
      .params
      .subscribe((params) => {
        console.log(`promo id | ${params['id']}`);
        if (parseInt(params['id'])) {
          this.promoId = params['id'];
          this.setExistingPromoData(null);
        } else {
          // this.staticService.loading.next(false);
        }
      });

  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
    this.subscription2?.unsubscribe();
    this.subscription3?.unsubscribe();
    this.subscription4?.unsubscribe();
    this.subscription5?.unsubscribe();
    this.subscription6?.unsubscribe();
    this.subscription7?.unsubscribe();
    this.paramSubscription?.unsubscribe();
  }

  private setExistingPromoData(promoDetail: Promotion | null) {
    // this.staticService.loading.next(true);

    if (promoDetail) {
      this.promoCode.setValue(promoDetail.promoCode);
      this.discountPrice.setValue(promoDetail.price);
      this.promoOffer.setValue(promoDetail.promoOffer);
      this.promoType.setValue(this.configData.promoTypes[this.configData.promoTypes.findIndex(value => value.toUpperCase() === promoDetail.promoType.toUpperCase())]);
      this.promoPlans.setValue(promoDetail.plans);

      this.loading = false;
    } else {
      this.subscription2 = this.promoService
        .findById(parseInt(this.promoId, 10))
        .subscribe({
          next: (promoDetail: Promotion) => {

            this.promotion = {
              promoType: promoDetail.promoType,
              promoCode: promoDetail.promoCode,
              promoOffer: promoDetail.promoOffer,
              price: promoDetail.price,
              plans: promoDetail.plans,
              id: promoDetail.id
            };

            this.promoCode.setValue(promoDetail.promoCode);
            this.discountPrice.setValue(promoDetail.price);
            this.promoOffer.setValue(promoDetail.promoOffer);
            this.promoType.setValue(this.configData.promoTypes[this.configData.promoTypes.findIndex(value => value.toUpperCase() === promoDetail.promoType.toUpperCase())]);

            if (this.filteredPlans) {
              for (let elt of this.promotion.plans as Plan[]) {
                this.filteredPlans.push(elt);
              }
            } else {
              this.filteredPlans = this.promotion.plans as Plan[];
            }


            this.promoPlans.setValue(promoDetail.plans);

            console.log(this.promoPlans.value);

            this.loading = false;
            // this.staticService.loading.next(false);
          },
          error: (err: HttpErrorResponse) => {
            return this.snackBarService.showSnackBar(JSON.parse(err?.error?.errorItems[0]?.message)?.message);
            this.loading = false;
            // this.staticService.loading.next(false);
          }
        });
    }
  }

  get promoCode(): AbstractControl {
    return this.promoForm.controls['promoCode'];
  }

  get discountPrice(): AbstractControl {
    return this.promoForm.controls['discountPrice'];
  }

  get promoOffer(): AbstractControl {
    return this.promoForm.controls['promoOffer'];
  }

  get promoType(): AbstractControl {
    return this.promoForm.controls['promoType'];
  }

  get promoPlans(): AbstractControl {
    return this.promoForm.controls['promoPlans'];
  }

  onPlanRemoved(plan: Plan) {
    const promoPlans = this.promoPlans.value;
    this.removeFirst(promoPlans, plan);
    this.promoPlans.setValue(promoPlans); // To trigger change detection
  }

  private removeFirst<T>(array: T[], toRemove: T): void {
    console.log('remove', toRemove, 'from', array);
    const index = array.indexOf(toRemove);
    if (index !== -1) {
      array.splice(index, 1);
    }
  }

  createForm(): void {
    this.promoForm = this.fb.group({
      promoCode: ['', [Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/)]],
      discountPrice: ['', [Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9,." "]*$/)]],
      promoOffer: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9,." "]*$/), Validators.maxLength(240)]],
      promoType: ['', [Validators.required, Validators.pattern(/^(\s+\S+\s*)*(?!\s).*$/), Validators.pattern(/^[a-zA-Z0-9" "]*$/), Validators.maxLength(240)]],
      promoPlans: ['', [Validators.required]],});

    this.promoForm.valueChanges
      .subscribe((val) => {
        console.log(val);
      });

  }

  savePromo(): void {
    // this.staticService.loading.next(true);

    this.postPromotion = {
      promoOffer: this.promoOffer?.value,
      promoCode: this.promoCode?.value,
      promoType: this.promoType?.value,
      price: this.discountPrice?.value,
      planIds: (this.promoPlans.value as Plan[]).map(value => value.id)
    };

    console.log(this.postPromotion);

    if (!this.promoId) {
      this.subscription3 = this.promoService
        .savePromotion(this.postPromotion)
        .subscribe({
          next: (resp: Promotion) => {
            console.log(resp);
            this.snackBarService.showSnackBar('Promotion Successfully saved', 'Success');
            this.router.navigate([`home/promotion-manager/promo-list`]);

            // this.staticService.loading.next(false);
            // return ids from posts to fsp, intermediary, key individual
          },
          error: (err) => {
            console.error(err);
            // this.staticService.loading.next(false);
            if (err.status === 409) {
              this.snackBarService.showSnackBar('Duplicate email', 'Please change the email to be unique');
            }
            return this.snackBarService.showSnackBar(JSON.parse(err?.error?.errorItems[0]?.message)?.message);
          },
          complete: () => {
            this.loading = false;
            this.subscription?.unsubscribe();
            // this.staticService.loading.next(false);
          }
        });
    } else {
      this.subscription4 = this.promoService
        .updatePromotion(this.postPromotion, parseInt(this.promoId, 10))
        .subscribe({
          next: () => {
            this.snackBarService.showSnackBar('Promotion Successfully edited', 'Success');
          },
          error: (err: any) => {
            console.error(err);
            // this.staticService.loading.next(false);
            if (err.status === 409) {
              this.snackBarService.showSnackBar('Duplicate email', 'Please change the email to be unique');
            }
            return this.snackBarService.showSnackBar(JSON.parse(err?.error?.errorItems[0]?.message)?.message);
          },
          complete: () => {
            this.loading = false;
            this.subscription?.unsubscribe();
            // this.staticService.loading.next(false);
          }
        });
    }

  }

  confirmPromoAdd() {

    // this.staticService.loading.next(true);

    const dialogData: DialogData = {
      buttonText: {
        confirm: 'Save Promo',
        cancel: 'Cancel'
      },
      description: 'Make sure all details are correct before saving.',
      failureMesssage: '',
      successMessage: '',
      title: `Are you sure you want to Save?`
    };

    this.subscription5 = this.dialogService
      .openDialog(dialogData)
      .afterClosed()
      .subscribe({
        next: (result) => {
          if (result) {
            this.savePromo();
          } else {
            // this.staticService.loading.next(false);
          }
        },
        error: () => {
          // this.staticService.loading.next(false);
          this.snackBarService.showSnackBar('Error occurred during confirmation dialog');
        }
      })
  }

  ngOnInit(): void {
    this.configData = this.staticService
      .configDetail
      .getValue();

    // populate mat-checkbox

    this.configData
      .serviceTypes
      .forEach(value => {

        const filterSubtask: Filter = {
          name: value,
          color: 'primary',
          completed: false,
          subtasks: undefined
        };

        this.serviceTypeFilter
          .subtasks
          ?.push(filterSubtask);
      });

    this.configData
      .serviceProviders
      .forEach(value => {

        const filterSubtask: Filter = {
          name: value,
          color: 'primary',
          completed: false,
          subtasks: undefined
        };

        this.serviceProviderFilter
          .subtasks
          ?.push(filterSubtask);
      });

    // In dev environment, fill with mock data
    if (!environment.production) {
      this.promoCode.setValue("OMINSURE");
      this.discountPrice.setValue("899.99");
      this.promoOffer.setValue("899.99"); // Can be string not just number
      this.promoType.setValue(this.configData.promoTypes[this.configData.promoTypes.findIndex(value => value.toUpperCase() === 'PRICE')]);
      // this.promoPlans.setValue(promoDetail.plans);
    }

  }

  checkNumbers($event: KeyboardEvent) {
    const numbersArr = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    console.log($event);
    if (numbersArr.indexOf($event.key) === -1) {
      return $event.preventDefault();
    }
  }

  checkNumbersPaste($event: ClipboardEvent) {
    const regexNumbers = new RegExp(/^[0-9]{1,45}$/);
    console.log($event);
    if (!regexNumbers.test(($event.clipboardData?.getData('text') as string))) {
      this.snackBarService.showSnackBar('You are attempting to paste a non digit-only value into a digit-only field');
      return $event.preventDefault();
    }
  }

  populatePlans() {

    this.filterOptions = {
      serviceProviders: (this.serviceProviderFilter.subtasks?.filter(val => val.completed).map(value => value.name) as string[]),
      serviceTypes: (this.serviceTypeFilter.subtasks?.filter(val => val.completed).map(value => value.name) as string[])
    };

    this.planService
      .getAllPlans(this.filterOptions)
      .subscribe({
        next: (plans: Plan[]) => {
          this.filteredPlans = plans;

          for (let elt of this.promotion.plans as Plan[]) {
            this.filteredPlans.push(elt);
          }

          console.log(this.filteredPlans);
        },
        error: (err: any) => {
          console.error(err);
          // this.staticService.loading.next(false);
          if (err.status === 409) {
            this.snackBarService.showSnackBar('Duplicate email', 'Please change the email to be unique');
          }
          return this.snackBarService.showSnackBar(JSON.parse(err?.error?.errorItems[0]?.message)?.message);
        },
      })
  }

  confirmPromoDelete() {
    const dialogData: DialogData = {
      buttonText: {
        confirm: 'Delete Promo',
        cancel: 'Cancel'
      },
      description: 'Deleting a Promo will remove it from the system.',
      failureMesssage: '',
      successMessage: '',
      title: `Are you sure you want to delete this promo?`
    };

    this.subscription5 = this.dialogService
      .openDialog(dialogData)
      .afterClosed()
      .subscribe({
        next: (result) => {
          if (result) {
            this.deletePromo();
          } else {
            // this.staticService.loading.next(false);
          }
        },
        error: () => {
          // this.staticService.loading.next(false);
          this.snackBarService.showSnackBar('Error occurred during confirmation dialog');
        }
      })
  }

  private deletePromo() {
    this.subscription7 = this.promoService
      .deletePromotion(parseInt(this.promoId, 10))
      .subscribe({
        next: () => {
          this.snackBarService.showSnackBar('Promotion successfully deleted', 'Success');
          this.router.navigate([`home/promotion-manager/promo-list`]);

        },
        error: (err: any) => {
          console.error(err);
          // this.staticService.loading.next(false);
          // if (err.status === 409) {
          //   this.snackBarService.showSnackBar('Duplicate email', 'Please change the email to be unique');
          // }
          return this.snackBarService.showSnackBar(JSON.parse(err?.error?.errorItems[0]?.message)?.message);
        },
        complete: () => {
          this.loading = false;
          this.subscription?.unsubscribe();
          // this.staticService.loading.next(false);
        }
      });
  }

  back() {
    if (this.promoId) {
      return this.router.navigate(['../../promo-list'], { relativeTo: this.route })
    }
    return this.router.navigate(['../promo-list'], { relativeTo: this.route })
  }
}
