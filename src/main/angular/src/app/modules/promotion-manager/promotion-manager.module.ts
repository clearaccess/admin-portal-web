import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PromotionManagerRoutingModule } from './promotion-manager-routing.module';
import { PromotionManagerComponent } from './promotion-manager.component';


@NgModule({
  declarations: [
    PromotionManagerComponent
  ],
  imports: [
    CommonModule,
    PromotionManagerRoutingModule
  ]
})
export class PromotionManagerModule { }
