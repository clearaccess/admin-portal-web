import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PromoListComponent } from './promo-list.component';

const routes: Routes = [{ path: '', component: PromoListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PromoListRoutingModule { }
