import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {HttpErrorResponse} from "@angular/common/http";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {MatSort} from "@angular/material/sort";
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {SnackbarService} from "../../../services/snackbar/snackbar.service";
import {StaticService} from "../../../services/static/static.service";
import {ConfigService} from "../../../services/config/config.service";
import {PromoService} from "../../../services/promo/promo.service";
import {PagingResponse} from "../../../interfaces/plan/PagingResponse";
import {PromoPlanResponse} from "../../../interfaces/plan/PromoPlanResponse";

@Component({
  selector: 'app-promo-list',
  templateUrl: './promo-list.component.html',
  styleUrls: ['./promo-list.component.scss']
})
export class PromoListComponent implements OnInit, OnDestroy {

  displayedColumns: string[] = ['id', 'promoCode', 'internetPlan', 'dateCreated', 'status', 'actions'];
  dataSource!: MatTableDataSource<PromoPlanResponse>;
  promoList!: PromoPlanResponse[];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  public limit: number = 5;
  public page: number = 0;
  public loading: boolean = true;
  public totalElements!: number;

  private subscription!: Subscription;
  private subscription2!: Subscription;
  private subscription3!: Subscription | undefined;
  private subscription4!: Subscription | undefined;

  constructor(
    private router: Router,
    private promoService: PromoService,
    private snackBarService: SnackbarService,
    private staticService: StaticService,
    private configService: ConfigService,
    private route: ActivatedRoute
  ) {
    this.getSavedLimit();
  }

  ngOnInit(): void {
    this.updateTable(false);
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
    this.subscription2?.unsubscribe();
    this.subscription3?.unsubscribe();
    this.subscription4?.unsubscribe();
  }

  getSavedLimit(): void {
    const limit = this.configService.getUserPreferenceFromStorage('pagination-limit');
    if (limit == null) {
      return;
    }
    if (parseInt(limit, 10) > 0) {
      this.limit = parseInt(limit, 10);
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  navigateToPromo(promo: PromoPlanResponse): void {
    console.log('Navigate to promo', promo);
    this.router.navigate([`../promo/${promo.id}`], { relativeTo: this.route });
  }

  addNewPromo(): void {
    console.log('Add new promo');
    this.router.navigate([`../promo`], { relativeTo: this.route });
  }

  public async asyncUpdateTable(updatePage?: boolean): Promise<any> {
    // this.staticService.loading.next(true);
    this.subscription = this.promoService
      .findAll(this.page, this.limit)
      .subscribe({
        next: (promoList: PagingResponse) => {
          this.promoList = promoList.content;
          this.dataSource = new MatTableDataSource(this.promoList);
          this.totalElements = promoList.totalElements;
          if (!updatePage) {
            this.paginator.pageIndex = 0;
          }

          return this.dataSource;
        },
        error: (err: HttpErrorResponse) => {
          console.error(err);
          // this.staticService.loading.next(false);
          return this.snackBarService.showSnackBar(JSON.parse(err?.error?.errorItems[0]?.message)?.message);
        }
      });
  }

  private updateTable(updatePage: boolean): void {
    // this.staticService.loading.next(true);
    this.subscription2 = this.promoService
      .findAll(this.page, this.limit)
      .subscribe({
        next: (promoList: PagingResponse) => {
          this.promoList = promoList.content;
          this.dataSource = new MatTableDataSource(this.promoList);
          // Paging
          this.dataSource.sort = this.sort;
          if (!updatePage) {
            this.paginator.pageIndex = 0;
          }
          this.totalElements = promoList.totalElements;
          return this.dataSource;
        },
        error: (err: HttpErrorResponse) => {
          console.error(err);
          this.loading = false;
          // this.staticService.loading.next(false);
          return this.snackBarService.showSnackBar('Something went wrong', 'Please try again');
        }
      });
  }

  async setPageSizeOptions(setPageSizeOptionsInput: PageEvent): Promise<PageEvent> {
    this.page = setPageSizeOptionsInput.pageIndex;
    this.limit = setPageSizeOptionsInput.pageSize;
    this.paginator.pageIndex = this.page;
    this.paginator.pageSize = this.limit;
    this.configService.setUserPreferenceFromStorage('pagination-limit', this.limit);
    await this.asyncUpdateTable(true);
    return setPageSizeOptionsInput;
  }

}
