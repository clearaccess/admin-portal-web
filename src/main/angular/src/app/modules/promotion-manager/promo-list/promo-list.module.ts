import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PromoListRoutingModule } from './promo-list-routing.module';
import { PromoListComponent } from './promo-list.component';
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatTableModule} from "@angular/material/table";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatCardModule} from "@angular/material/card";
import {SharedModule} from "../../../shared/shared.module";
import {PromotionApiClass} from "../../../api/promotion.api.class";
import {PromoService} from "../../../services/promo/promo.service";


@NgModule({
  declarations: [
    PromoListComponent
  ],
  imports: [
    PromoListRoutingModule,
    MatPaginatorModule,
    MatTooltipModule,
    MatTableModule,
    SharedModule
  ],
  providers: [
    PromotionApiClass,
    PromoService
  ]
})
export class PromoListModule { }
