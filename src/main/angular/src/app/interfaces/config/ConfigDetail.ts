import {EntityDetails} from "../entity/EntityDetails";

export interface ConfigDetail {
  promoTypes: string[];
  serviceTypes: string[];
  serviceProviders: string[];
}
