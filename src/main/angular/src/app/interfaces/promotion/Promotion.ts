import {Plan} from "../plan/Plan";

export interface Promotion {
  id: number;
  price: number;
  promoCode: string;
  promoOffer: string;
  promoType: string;
  plan?: Plan;
  plans?: Plan[];
}
