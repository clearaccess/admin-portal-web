export interface PostPromotion {
  // id: number;
  price: number;
  promoCode: string;
  promoOffer: string;
  promoType: string;
  planIds: number[];
}
