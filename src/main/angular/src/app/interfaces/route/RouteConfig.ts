import {UserRole} from "../UserRole";

export interface RouteConfig {
  userRoles: UserRole;
}
