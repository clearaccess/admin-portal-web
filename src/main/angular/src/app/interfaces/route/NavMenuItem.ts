export interface NavMenuItem {
  name: string;
  code: string;
  image: string;
  headerText: string;
  active: boolean;
  route: string;
}
