export interface Route {
  name: string;
  image?: string;
  code: string;
  route?: string;
  parent?: string;
  active: boolean;
}
