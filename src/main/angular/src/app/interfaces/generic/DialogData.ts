export interface DialogData {
  buttonText: {
    confirm: string;
    cancel: string;
  };
  description: string;
  title: string;
  successMessage: string;
  failureMesssage: string;
}
