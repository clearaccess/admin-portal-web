export interface EntityDetails {
  id: number;
  name: string;
  code?: string;
}
