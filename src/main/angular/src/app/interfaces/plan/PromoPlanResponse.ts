export interface PromoPlanResponse {
  id: number;
  promoCode: string;
  internetPlan: string;
  dateCreated: string;
  status: string;
}
