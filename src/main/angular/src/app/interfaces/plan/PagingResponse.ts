export interface PagingResponse {
  content: any[];
  totalElements: number;
}
