export interface Plan {
  id: number;
  actions: string;
  connectivityProvider: string;
  installOptions: string;
  onlyPromo: boolean;
  price: number;
  serviceDetails: string;
  serviceType: string;
  shortcode: string;
  title: string;
  dateCreated: string;
  status: string;
}
