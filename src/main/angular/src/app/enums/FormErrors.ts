export enum FormErrors {
  REQUIRED = 'Required',
  MINLENGTH = 'MINLENGTH',
  MAXLENGTH = 'MAXLENGTH',
  EMAIL = 'EMAIL',
  PATTERN = 'PATTERN',
  MAX = 'MAX',
  MIN = 'MIN'
}
