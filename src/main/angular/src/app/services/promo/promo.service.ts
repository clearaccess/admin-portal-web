import { Injectable } from '@angular/core';
import {PromotionApiClass} from "../../api/promotion.api.class";
import {Observable} from "rxjs";
import {PostPromotion} from "../../interfaces/promotion/PostPromotion";

@Injectable({
  providedIn: 'root'
})
export class PromoService {

  constructor(
    private promoApi: PromotionApiClass
  ) { }

  public findById(id: number): Observable<any> {
    return this.promoApi.findById(id);
  }

  public findAll(page: number, limit: number): Observable<any> {
    return this.promoApi.findAll(page, limit);
  }

  public savePromotion(postPromotion: PostPromotion): Observable<any> {
    return this.promoApi.savePromotion(postPromotion);
  }

  public deletePromotion(id: number): Observable<any> {
    return this.promoApi.deletePromotion(id);
  }

  updatePromotion(postPromotion: PostPromotion, id: number) {
    return this.promoApi.updatePromotion(postPromotion, id);
  }
}
