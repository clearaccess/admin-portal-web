import { Injectable } from '@angular/core';
import {AbstractControl} from "@angular/forms";
import {FormErrors} from "../../enums/FormErrors";

export const FormControlErrors = [
  {
    code: FormErrors.REQUIRED,
    errorMsg: 'Please enter a value'
  },
  {
    code: FormErrors.MINLENGTH,
    errorMsg: 'This field is too short, please enter a greater value'
  },
  {
    code: FormErrors.MAXLENGTH,
    errorMsg: 'This field is too long, please enter a shorter value'
  },
  {
    code: FormErrors.EMAIL,
    errorMsg: 'Please enter a valid email'
  },
  {
    code: FormErrors.PATTERN,
    errorMsg: 'Invalid format'
  },
  {
    code: FormErrors.MAX,
    errorMsg: 'Value is too great'
  },
  {
    code: FormErrors.MIN,
    errorMsg: 'Value is not enough'
  }
];

export const FormControlErrorsArr = ['REQUIRED', 'MINLENGTH', 'MAXLENGTH', 'EMAIL', 'PATTERN', 'MAX', 'MIN'];

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  public getError(control: AbstractControl): string | null {
    if (control.errors) {
      for (const elt in control.errors) {
        if (!elt) {
          return null;
        }
        if (FormControlErrorsArr.indexOf(elt.toUpperCase()) > -1) {
          return FormControlErrors.filter(value => value.code.toLowerCase() === elt.toLowerCase())[0].errorMsg;
        }
      }
    }
    return null;
  }

  constructor() { }
}
