import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {SessionApiClass} from "../../api/session.api.class";
import { JwtHelperService } from "@auth0/angular-jwt";

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor(private sessionApi: SessionApiClass) { }

  public retrieveSession(): Observable<any> {
    return this.sessionApi.retrieveSession();
  }

  public checkAuthoritiesSessionGuard(route: string): Observable<any> {
    return this.sessionApi.checkAuthoritiesSessionGuard(route);
  }

  logout(): Observable<any> {
    return this.sessionApi.logout();
  }

  public isSessionValid(sessionToken: string): Promise<boolean> {
    const helper = new JwtHelperService();

    const decodedToken = helper.decodeToken(sessionToken);
    const expirationDate = helper.getTokenExpirationDate(sessionToken);
    const isExpired = helper.isTokenExpired(sessionToken);

    console.log(sessionToken, isExpired);

    return new Promise((resolve, reject) => {
      resolve(!isExpired);
    });

  }

  public getSessionRoutes(): Observable<any> {
    return this.sessionApi.getSessionRoutes();
  }

}
