import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Token} from "../../interfaces/Token";
import {AuthApiClass} from "../../api/auth.api.class";
import {Login} from "../../interfaces/Login";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private authApi: AuthApiClass
  ) {
  }

  login(login: Login): Observable<any> {
    return this.authApi.login(login);
  }

}
