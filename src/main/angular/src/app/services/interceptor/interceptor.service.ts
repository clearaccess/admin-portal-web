import { Injectable } from '@angular/core';
import {HttpHandler, HttpRequest} from "@angular/common/http";
import {StaticService} from "../static/static.service";
import {finalize} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class InterceptorService {

  private totalRequests = 0;

  constructor(
    private staticService: StaticService
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler) {

    if (request && next) {
      if (request.url.indexOf("/login") === -1) {
        request = request.clone({
          setHeaders: {
            // 'Content-Type' : 'application/json; charset=utf-8',
            // 'Accept'       : 'application/json',
            'Authorization': `Bearer ${sessionStorage.getItem('session-token')}`,
          },
        });
      }

    }

    this.totalRequests++;
    this.staticService.setLoading(true);

    return next.handle(request).pipe(
      finalize(() => {
        this.totalRequests--;
        if (this.totalRequests === 0) {
          this.staticService.setLoading(false);
        }
      })
    );
  }
}
