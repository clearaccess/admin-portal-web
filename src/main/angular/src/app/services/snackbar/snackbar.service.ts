import { Injectable } from '@angular/core';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarRef,
  MatSnackBarVerticalPosition
} from "@angular/material/snack-bar";

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

  constructor(private snackBar: MatSnackBar) { }

  showSnackBar(
    message: string,
    action?: string,
    verticalPosition?: MatSnackBarVerticalPosition,
    horizontalPosition?: MatSnackBarHorizontalPosition,
    timer?: number
  ): MatSnackBarRef<any> {
    return this.snackBar.open(message, action ? action : '', {
      duration: timer ? timer : 6000,
      horizontalPosition: horizontalPosition ? horizontalPosition : 'center',
      verticalPosition: verticalPosition ? verticalPosition : 'bottom',
      panelClass: ['kp-snackbar']
    });
  }

}
