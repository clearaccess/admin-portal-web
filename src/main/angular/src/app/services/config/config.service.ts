import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {ConfigApiClass} from "../../api/config.api.class";

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor(
    private configApi: ConfigApiClass
  ) { }

  getAllConfigData(): Observable<any> {
    return this.configApi.getAllConfigData();
  }

  getUserPreferenceFromStorage(key: string): string | null {
    return localStorage.getItem(key);
  }

  setUserPreferenceFromStorage(key: string, value: string | number | object): void {
    let saveValue: string;
    saveValue = value.toString();
    if (typeof value === 'object') {
      saveValue = JSON.stringify(value);
    }
    return localStorage.setItem(key, saveValue);
  }

}
