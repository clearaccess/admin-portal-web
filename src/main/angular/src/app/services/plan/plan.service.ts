import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {PlanFilterOptions} from "../../interfaces/filter/PlanFilterOptions";
import {PlanApiClass} from "../../api/plan.api.class";

@Injectable({
  providedIn: 'root'
})
export class PlanService {

  constructor(
    private planApi: PlanApiClass
  ) { }

  public getAllPlansPaging(page: number, limit: number): Observable<any> {
    return this.planApi.getAllPlansPaging(page, limit);
  }

  public getAllPlans(filterOptions: PlanFilterOptions): Observable<any> {
    return this.planApi.getAllPlans(filterOptions);
  }
}
