import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {DialogData} from "../../../interfaces/generic/DialogData";

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public matDialogData: DialogData,
    private matDialogRef: MatDialogRef<any>
  ) { }

  ngOnInit(): void {
    console.log(this.matDialogRef);
  }

  closeDialog() {
    this.matDialogRef.close();
  }

  save(): void {
    this.matDialogRef.close(true);
  }

}
