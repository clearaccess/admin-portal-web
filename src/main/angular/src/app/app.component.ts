import {AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from "./services/auth/auth.service";
import {NavigationStart, Router} from "@angular/router";
import {filter} from "rxjs/operators";
import {Token} from "./interfaces/Token";
import {HttpErrorResponse} from "@angular/common/http";
import {SessionService} from "./services/session/session.service";
import {SnackbarService} from "./services/snackbar/snackbar.service";
import {NgxsOnInit} from "@ngxs/store";
import {StaticService} from "./services/static/static.service";
import {Subscription} from "rxjs";
import {ConfigService} from "./services/config/config.service";
import {ConfigDetail} from "./interfaces/config/ConfigDetail";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {
  title = 'admin-portal-angular';

  public isLoggedIn = false;
  private loadingSubscription!: Subscription;
  public loading: boolean = true;

  constructor
  (
    private router: Router,
    private sessionService: SessionService,
    private snackBarService: SnackbarService,
    public staticService: StaticService,
    private configService: ConfigService,
    private snackbarService: SnackbarService,
    private cdr: ChangeDetectorRef
  ) {

    // Router
    router.events
      .pipe(filter(event => event instanceof NavigationStart))
      // @ts-ignore
      .subscribe((event: NavigationStart) => {
        // Dispatch current route to store for use in other components.
        // this.store.dispatch(new SetRoute(event.url));
        // If LoginComponent active, don't do session retrieval call.
        console.log(event);
        if (!(event.url === '/login')) {
          // if (event.url !== '/login') {
          this.sessionService.retrieveSession()
            .subscribe((token: Token) => {
              // this.store.dispatch(new SetToken(token));
              if (event.url === '/login') {
                this.router.navigate(['/home']);
              }
            }, (err: HttpErrorResponse) => {
              this.router.navigate(['/login']);
              this.snackBarService.showSnackBar(err.error.message);
            });
          // }
        }
      });

  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.loadingSubscription = this.staticService
      .isLoading$
      .subscribe({
        next: () => this.cdr.detectChanges(),
        error: () => this.cdr.detectChanges()
      });
  }

  ngOnDestroy() {
    this.loadingSubscription.unsubscribe();
  }

}
