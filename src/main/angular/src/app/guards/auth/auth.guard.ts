import { Injectable } from '@angular/core';
import {CanLoad, Route, Router, UrlSegment, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {SessionService} from "../../services/session/session.service";
import {HttpErrorResponse} from "@angular/common/http";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad {

  constructor(
    private sessionService: SessionService,
    private router: Router
  ) {
  }

  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    // return new Promise((resolve, reject) => {
    //   this.sessionService.checkAuthoritiesSessionGuard((route.path as string))
    //     .subscribe(() => {
    //       resolve(true);
    //     }, (err: HttpErrorResponse) => {
    //       if (environment.production) {
    //         reject(window.location.href = `${window.location.host}/login`);
    //       } else {
    //         reject(window.location.href = `localhost:7004/login`);
    //       }
    //     });
    // });

    const sessionToken = sessionStorage.getItem('session-token');
    console.log('guard session token', sessionToken);

    if (!sessionToken) {
      this.router.navigate(['login']);
      return false;
    }

    return this.sessionService.isSessionValid(sessionToken);

  }
}
