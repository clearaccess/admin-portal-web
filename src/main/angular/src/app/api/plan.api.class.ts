import {Injectable} from "@angular/core";
import {HttpClient, HttpHandler} from "@angular/common/http";
import {Observable} from "rxjs";
import {PlanFilterOptions} from "../interfaces/filter/PlanFilterOptions";
import {environment} from "../../environments/environment";

@Injectable()
export class PlanApiClass extends HttpClient {

  private baseUrl = environment.baseUrl + '/plan';

  public constructor(handler: HttpHandler) {
    super(handler);
  }

  public getAllPlansPaging(page: number, limit: number): Observable<any> {
    const plansUrl = `${this.baseUrl}/${page}/${limit}`;
    return super.get(plansUrl);
  }

  public getAllPlans(filterOptions: PlanFilterOptions): Observable<any> {
    const plansUrl = `${this.baseUrl}/filter`;
    return super.post(plansUrl, filterOptions);
  }

}
