import {Injectable} from "@angular/core";
import {HttpClient, HttpHandler} from "@angular/common/http";
import {Observable} from "rxjs";
import {Login} from "../interfaces/Login";
import {environment} from "../../environments/environment";

@Injectable()
export class AuthApiClass extends HttpClient {

  baseUrl = environment.baseUrl + '/login-uri/getToken';

  constructor(private httpHandler: HttpHandler) {
    super(httpHandler);
  }

  login(login: Login): Observable<any> {
    return super.post(this.baseUrl, login);
  }
}
