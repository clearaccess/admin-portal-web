import {Injectable} from "@angular/core";
import {HttpClient, HttpHandler} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";

@Injectable()
export class ConfigApiClass extends HttpClient {

  private baseUrl = environment.baseUrl + '/config';

  public constructor(handler: HttpHandler) {
    super(handler);
  }

  public getAllConfigData(): Observable<any> {
    const sessionUrl = `${this.baseUrl}/retrieve`;
    return super.get(sessionUrl);
  }

}
