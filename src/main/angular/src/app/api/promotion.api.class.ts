import {Injectable} from "@angular/core";
import {HttpClient, HttpHandler} from "@angular/common/http";
import {Observable} from "rxjs";
import {PostPromotion} from "../interfaces/promotion/PostPromotion";
import {environment} from "../../environments/environment";

@Injectable()
export class PromotionApiClass extends HttpClient {

  private baseUrl = environment.baseUrl + '/promotion';

  public constructor(handler: HttpHandler) {
    super(handler);
  }

  public findById(id: number): Observable<any> {
    const promotionUrl = `${this.baseUrl}/${id}`;
    return super.get(promotionUrl);
  }

  public findAll(page: number, limit: number): Observable<any> {
    const promotionUrl = `${this.baseUrl}/${page}/${limit}`;
    return super.get(promotionUrl);
  }

  public savePromotion(postPromotion: PostPromotion): Observable<any> {
    const promotionUrl = `${this.baseUrl}`;
    return super.post(promotionUrl, postPromotion);
  }

  public deletePromotion(id: number): Observable<any> {
    const promotionUrl = `${this.baseUrl}/${id}`;
    return super.delete(promotionUrl);
  }

  updatePromotion(postPromotion: PostPromotion, id: number) {
    const promotionUrl = `${this.baseUrl}/${id}`;
    return super.put(promotionUrl, postPromotion);
  }
}
