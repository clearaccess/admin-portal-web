import {HttpClient, HttpHandler} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";

@Injectable()
export class SessionApiClass extends HttpClient {

  private baseUrl = environment.baseUrl + '/session';

  public constructor(handler: HttpHandler) {
    super(handler);
  }

  public retrieveSession(): Observable<any> {
    const sessionUrl = `${this.baseUrl}/retrieve`;
    return super.get(sessionUrl);
  }

  retrieveSessionVerify(): Observable<any> {
    const sessionUrl = `${this.baseUrl}/retrieve-verify`;
    return super.get(sessionUrl);
  }

  checkAuthoritiesSessionGuard(route: string): Observable<any> {
    const sessionUrl = `${this.baseUrl}/check-authorities`;
    return super.post(sessionUrl, { route });
  }

  logout(): Observable<any> {
    const sessionUrl = `${this.baseUrl}/logout`;
    return super.get(sessionUrl);
  }

  getSessionRoutes(): Observable<any> {
    const sessionUrl = `${this.baseUrl}/routes`;
    return super.get(sessionUrl);
  }
}
