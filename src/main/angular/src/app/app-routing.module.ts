import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AuthGuard} from "./guards/auth/auth.guard";
import {AppComponent} from "./app.component";

const routes: Routes = [
    {
      path: 'home',
      loadChildren: () => import('./home/home.module').then(m => m.HomeModule),
      canLoad: [AuthGuard]
    },
    {
      path: '',
      component: AppComponent
    },
    {
      path: 'login',
      loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
    },
    {
      path: '**',
      component: AppComponent
    },
    // {
    //   path: 'login',
    //   loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
    // }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
