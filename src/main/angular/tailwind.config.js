module.exports = {
  purge: ['./src/**/*.{html,ts}'],
  darkMode: false,
  content: ['./src/**/*.{html,ts}'],
  theme: {
    screens: {
      'xs': '320px',
      // => @media (min-width: 320px) { ... }

      'sm': '640px',
      // => @media (min-width: 640px) { ... }

      'md': '768px',
      // => @media (min-width: 768px) { ... }

      'lg': '1024px',
      // => @media (min-width: 1024px) { ... }

      'xl': '1280px',
      // => @media (min-width: 1280px) { ... }

      '2xl': '1536px',
      // => @media (min-width: 1536px) { ... }

      '3xl': '1920px',
      // => @media (min-width: 1921px) { ... }

      '4xl': '2200px',
      // => @media (min-width: 1921px) { ... }
      '5xl': '3800px',
      // => @media (min-width: 3801px) { ... }
    },
    backgroundColor: {
      'background': '#F2F2F2',
      'white': '#FFFFFF',
      'primary': '#0E89CB',
      'disabled': '#E6E6E6',
    },
    colors: {
      'login-title': '#5A5A5A',
      'primary': '#0E89CB',
      'secondary': '#00000029',
      'secondary-grey': '#D8F6FF',
      'secondary-black': '#000000',
      'success': '#1BB800',
      'grey-tones-1': '#FAFAFA',
      'grey-tones-2': '#EBEBEB',
      'grey-tones-3': '#E4E4E4',
      'grey-tones-4': '#D0D3D4',
      'grey-tones-5': '#A2AAAD',
      'grey-tones-6': '#425563',
      'disabled': '#E6E6E6',
      'secondary-extended-1': '#FF9399',
      'secondary-extended-2': '#FFCACC',
      'secondary-extended-3': '#FF9466',
      'table-hover': '#D8F6FF',
      'form-style': '#2DC7F6',
      'table-text': '#333333',
      'white': '#FFFFFF',
      'grey': '#FAFAFA',
      'form-grey': 'rgb(255 237 237)',
      'table-font': '#5A5A5A'
    },
    fontFamily: {
      'poppins-font': ['Poppins']
    },
    fill: {
      'primary': '#0E89CB',
      'secondary': '#00000029',
      'secondary-grey': '#5C5C5C',
      'secondary-black': '#000000',
      'disabled-text': '#B9B9B9',
      'grey-tones-1': '#FAFAFA',
      'grey-tones-2': '#EBEBEB',
      'grey-tones-3': '#E4E4E4',
      'grey-tones-4': '#D0D3D4',
      'grey-tones-5': '#A2AAAD',
      'grey-tones-6': '#425563',
      'success': '#1BB800',
      'disabled': '#E6E6E6',
      'secondary-extended-1': '#FF9399',
      'secondary-extended-2': '#FFCACC',
      'secondary-extended-3': '#FF9466',
      'table-hover': '#D8F6FF',
      'form-style': '#2DC7F6',
      'table-text': '#333333',
      'white': '#FFFFFF',
      'grey': '#FAFAFA',
      'form-grey': 'rgb(255 237 237)',
      'background': '#F2F2F2',
      'table-font': '#5A5A5A'
    },
    extend: {
      colors: {
        'loading-success': '#1BB800',
        'loading-danger': '#D6001C',
        'loading-progress': '#f2f2f2'
      },
      padding: {
        '1/10': '10%',
        '1.4/10': '14%',
        '2/10': '20%',
        '3/10': '30%',
        '4/10': '40%',
        '5/10': '50%',
        '6/10': '60%',
        '7/10': '70%',
        '8/10': '80%',
        '9/10': '90%',
        '10/10': '100%'
      },
      maxHeight: {
        '1/2': '50%',
        '2/3': '66%',
        '3/4': '75%',
        '75': '75px',
        '100': '100px',
        '150': '150px',
        '200': '200px',
        '250': '250px',
        '500': '500px',
        '600': '600px',
        '650': '650px',
        '700': '700px',
        '750': '750px'
      },
      maxWidth: {
        '1/2': '50%',
        '2/3': '66%',
        '3/4': '75%',
        '50': '50px',
        '75': '75px',
        '100': '100px',
        '150': '150px',
        '200': '200px',
        '250': '250px',
        '500': '500px',
        '600': '600px',
        '650': '650px',
        '700': '700px',
        '750': '750px'
      },
      width: {
        '22': '22%',
        '23': '23%',
        '24': '24%',
        '48': '48%',
        '49': '49%'
      }
    },
  },
  variants: {
    extend: {
      backgroundColor: ['active'],
      margin: ['last'],
      padding: ['hover'],
      width: ['hover'],
      translate: ['hover'],
      transform: ['hover'],
      colors: {
        'background': '#F2F2F2'
      }
    },
  },
  plugins: [
    require('./src/app/plugins/tailwind.plugin')(),
  ],
  important: true
};
